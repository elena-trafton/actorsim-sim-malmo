package nrl.actorsim.malmo.goals;

import nrl.actorsim.domain.ProgramStatement;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalnetwork.GoalNetworkNode;
import org.slf4j.LoggerFactory;

import static nrl.actorsim.domain.PredicateStatement.ALWAYS_TRUE_STATEMENT;

public class MalmoGoal extends GoalNetworkNode<ProgramStatement> {
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(MalmoGoal.class);

    public static MalmoGoal NULL_MALMO_GOAL = new MalmoGoal(ALWAYS_TRUE_STATEMENT);

    public MalmoGoal(Statement statement) {
        super(statement);
    }

    public MalmoGoal(Statement statement, boolean isPrimitive) {
        super(statement, isPrimitive);
    }

    public MalmoGoal(GoalNetworkNode<ProgramStatement> other) {
        super(other);
    }

    @Override
    public Statement getStored() {
        return (Statement) super.getStored();
    }
}
