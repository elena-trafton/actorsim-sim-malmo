import datetime
import math
import random
import sys
import time

from action import ActionBase, ComplexAction_DEPRECATED
from character.character import Character, find_best_look_location, \
    find_best_entity_look_location, normalize_angle
from character.memory import NULL_MEMORY_OBJECT
from entity import NullEntity
from location import Location, NullLocation, NULL_LOCATION, EntityLocation, BlockLocation, DropLocation, MobLocation
from loggers import MalmoLoggers
from malmo_types import MalmoTypeManager
from mission import ObservationGridDetails
from area import Area

logger = MalmoLoggers.action_logger


class Pause(ActionBase):
    def __init__(self, character: Character, time_in_ms):
        ActionBase.__init__(self, "pause({})".format(time_in_ms))
        self.character = character
        time_as_int = time_in_ms
        if isinstance(time_in_ms, str):
            time_as_int = int(float(time_in_ms))
        self.time_in_seconds = time_as_int / 1000
        self.start_time = None

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if self.start_time is None:
            self.start_time = datetime.datetime.now()
        else:

            now = datetime.datetime.now()
            time_delta = now - self.start_time
            logger.debug("{}: Run for {} seconds".format(self.log_name, time_delta.total_seconds()))
            if time_delta.total_seconds() >= self.time_in_seconds:
                self._completed = True

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class PauseUntilEntityDead(ActionBase):
    '''
    Please note: This should only be used when the expected outcome of the previous action
    is a dead entity.

    Entity pauses for a certain number of game ticks (is this the right terminology).
    '''
    def __init__(self, character: Character, entity, max_num_executions: int=100):
        ActionBase.__init__(self, "pause_until_entity_dead")
        self.character = character
        self.entity = entity
        self.max_num_executions = max_num_executions
        self.current_num_executions = 0
        self.initial_state_index = 0

    def execute(self):
        '''Waits either for minecraft engine to remove entity or some maximum number
        of action executions.
        '''
        logger.debug("{}: starting execute ".format(self.log_name))
        if self.current_num_executions >= self.max_num_executions:
            logger.debug("{}: Max number of executions of action reached waiting for "
                         "entity to die.".format(self.log_name))
            self._completed = True
            return
        else:
            if self.current_num_executions == 0:
                self.initial_state_index = self.character.get_latest_state().state_index

            readonly_memory_object = self.character.get_observation(self.entity.id)
            if not readonly_memory_object.is_valid_observation or readonly_memory_object == NULL_MEMORY_OBJECT:
                latest_state_index = self.character.get_latest_state().state_index
                elapsed_num_states = latest_state_index - self.initial_state_index
                logger.debug("{}: Entity {} is now dead. "
                             "Entity died at state {} after {} states elapsed. "
                             "Num action executions: {}".format(self.log_name,
                                                                self.entity,
                                                                latest_state_index,
                                                                elapsed_num_states,
                                                                self.current_num_executions))
                self._completed = True
            else:
                logger.debug("{}: Entity {} is still alive. Waiting until environment updates. "
                             "State Index: {}".format(self.log_name, self.entity,
                                                      self.character.get_latest_state().state_index))
                self.current_num_executions += 1
        logger.debug("{}: end of execute".format(self.log_name))


class PauseUntilEntityUpdate(ActionBase):
    def __init__(self, character: Character):
        ActionBase.__init__(self, "pause_until_entity_update")
        self.character = character
        self.start_entity_update_index = 0

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        latest_entity_update = self.character.get_last_entity_update_time()
        if self.start_entity_update_index == 0:
            self.start_entity_update_index = latest_entity_update
        else:
            if self.start_entity_update_index < latest_entity_update:
                self._completed = True

        logger.debug("{}: end of execute start_index:{} latest:{} completed:{}".format(self.log_name, self.start_entity_update_index, latest_entity_update, self._completed))


class MoveToStaticTarget(ActionBase):
    def __init__(self, character : Character,
                 target: Location = NULL_LOCATION,
                 distance_threshold=0.25,
                 distance_scale=3,
                 min_movement_threshold : float = 0.1,
                 pitch_to_target=False,
                 block_size=1.0):
        ActionBase.__init__(self, "move_to_static:{}".format(target))
        self.target = target # type: Location
        self.adjusted_target = target.copy()
        self.min_movement_threshold = min_movement_threshold
        self.character = character # type: Character
        self.pitch_to_target = pitch_to_target
        self.distance_threshold = distance_threshold
        self.distance_scale = distance_scale
        self.block_size=block_size

    @staticmethod
    def create_collect_from_location(character: Character):
        return MoveToStaticTarget(character,
                                  Location(),
                                  distance_threshold=0.3,
                                  distance_scale=1,
                                  pitch_to_target=True)

    def reset_target(self):
        self.target = NULL_LOCATION
        self.name = "move_to_static:{}".format(self.target)
        self.log_name = self.name

    def set_target(self, target):
        self.target = target
        self.name = "move_to_static:{}".format(self.target)
        self.log_name = self.name

    def execute(self):
        logger.debug("{}: starting execute cur_loc:{}".format(self.log_name, self.character.get_location()))

        if isinstance(self.target, NullLocation):
            logger.debug("{]: Refusing to move to a NullLocation!".format(self.log_name))
            return

        if isinstance(self.target, EntityLocation):
            logger.debug("{}: target is an entity, ensuring the observation is valid".format(self.log_name))
            memory_object = self.character.get_observation(self.target.id)
            if not memory_object.is_valid_observation:
                logger.debug("{}: target {} is no longer valid, marking action complete".format(self.log_name, self.target))
                self.target = NULL_LOCATION
                self._completed = True
                return

        adjusted_x, adjusted_z, adjusted_yaw = self.character.find_best_location(self.target, block_size=self.block_size)
        self.adjusted_target.x = adjusted_x
        self.adjusted_target.y = self.target.y
        self.adjusted_target.z = adjusted_z

        block_type = self.character.get_block_type_at(self.target)
        type_str = ""
        if block_type is not None:
            type_str = "({})".format(block_type)
        self.name = "move_to_static{}:{}".format(type_str, self.adjusted_target)

        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        translation_velocity, rotation_velocity = self.character.calc_move_and_turn_velocities(distance, yaw,
                                                                                distance_threshold=self.distance_threshold,
                                                                                distance_scale=self.distance_scale)

        logger.debug("{}: going to adjusted target {}; yaw:{} adjusted_yaw:{}".format(self.log_name, self.adjusted_target, yaw, adjusted_yaw))
        logger.debug("{}: distance:{:0.3f} threshold:{}".format(self.log_name, distance, self.distance_threshold))

        if distance >= self.distance_threshold:
            logger.debug("{}: Command translation_velocity: {} rotation:{}".format(self.log_name, translation_velocity, rotation_velocity))
            translation = translation_velocity
            self.character.command.set_move(translation)
            self.character.command.set_turn(rotation_velocity)
            self._completed = False
        else:
            logger.debug("{}: moveToStaticTarget is done".format(self.log_name))
            self.character.command.reset_move()
            self.character.command.reset_turn()
            self._completed = True

        if self.pitch_to_target:
            pitch_vel = self.character.calc_pitch_velocity(pitch)
            if abs(pitch_vel) > self.min_movement_threshold:
                self.character.command.set_pitch(pitch_vel)
            else:
                self.character.command.reset_pitch()

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class MoveSafelyToTarget(ActionBase):
    def __init__(self, character : Character,
                 target: Location = NULL_LOCATION,
                 distance_threshold=0.25,
                 distance_scale=3,
                 min_movement_threshold : float = 0.1,
                 pitch_to_target=False,
                 block_size=1.0):
        ActionBase.__init__(self, "move_to_static:{}".format(target))
        self.target = target # type: Location
        self.adjusted_target = target.copy()
        self.min_movement_threshold = min_movement_threshold
        self.character = character # type: Character
        self.pitch_to_target = pitch_to_target
        self.distance_threshold = distance_threshold
        self.distance_scale = distance_scale
        self.block_size=block_size

    def reset_target(self):
        self.target = NULL_LOCATION
        self.name = "move_to_static:{}".format(self.target)
        self.log_name = self.name

    def set_target(self, target):
        self.target = target
        self.name = "move_to_static:{}".format(self.target)
        self.log_name = self.name

    def execute(self):
        logger.debug("{}: starting execute cur_loc:{}".format(self.log_name, self.character.get_location()))

        if isinstance(self.target, NullLocation):
            logger.debug("{]: Refusing to move to a NullLocation!".format(self.log_name))
            return

        if isinstance(self.target, EntityLocation):
            logger.debug("{}: target is an entity, ensuring the observation is valid".format(self.log_name))
            memory_object = self.character.get_observation(self.target.id)
            if not memory_object.is_valid_observation:
                logger.debug("{}: target {} is no longer valid, marking action complete".format(self.log_name, self.target))
                self.target = NULL_LOCATION
                self._completed = True
                return

        adjusted_x, adjusted_z, adjusted_yaw = self.character.find_best_location(self.target, block_size=self.block_size)
        self.adjusted_target.x = adjusted_x
        self.adjusted_target.y = self.target.y
        self.adjusted_target.z = adjusted_z

        block_type = self.character.get_block_type_at(self.target)
        type_str = ""
        if block_type is not None:
            type_str = "({})".format(block_type)
        self.name = "move_to_static{}:{}".format(type_str, self.adjusted_target)

        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        translation_velocity, rotation_velocity = self.character.calc_move_and_turn_velocities(distance, yaw,
                                                                                distance_threshold=self.distance_threshold,
                                                                                distance_scale=self.distance_scale)

        logger.debug("{}: going to adjusted target {}; yaw:{} adjusted_yaw:{}".format(self.log_name, self.adjusted_target, yaw, adjusted_yaw))
        logger.debug("{}: distance:{:0.3f} threshold:{}".format(self.log_name, distance, self.distance_threshold))

        onTarget_x = math.floor(self.target.x) == math.floor(self.character.get_location().x)
        onTarget_z = math.floor(self.target.z) == math.floor(self.character.get_location().z)
        onTarget = onTarget_x and onTarget_z

        if distance >= self.distance_threshold and not onTarget:
            self.character
            logger.debug("{}: Command translation_velocity: {} rotation:{}".format(self.log_name, translation_velocity, rotation_velocity))
            translation = translation_velocity
            if self.character.facing_danger():
                self.character.command.reset_move()
            else:
                self.character.command.set_move(translation)
            self.character.command.set_turn(rotation_velocity)
            self._completed = False
        else:
            logger.debug("{}: moveToStaticTarget is done".format(self.log_name))
            self.character.command.reset_move()
            self.character.command.reset_turn()
            self._completed = True

        if self.pitch_to_target:
            pitch_vel = self.character.calc_pitch_velocity(pitch)
            if abs(pitch_vel) > self.min_movement_threshold:
                self.character.command.set_pitch(pitch_vel)
            else:
                self.character.command.reset_pitch()

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class MoveToEntity(ActionBase):
    """Move to an entity.
       Using a lower distance_scale results in faster movement.
    """
    def __init__(self, character: Character,
                 entity_type: str,
                 proposed_target: MobLocation = NULL_LOCATION,
                 distance_threshold: float = 2.0,
                 min_movement_threshold : float = 0.1,
                 target_scale=1,
                 distance_scale=3,
                 pitch_to_target=True,
                 complete_on_execute_if_no_entity_found=False):
        ActionBase.__init__(self, "move_to_entity({})".format(entity_type))
        self.entity_type = entity_type
        self.proposed_target = proposed_target
        self.target = NULL_LOCATION
        self.reset_target()
        self.entity_location = NULL_LOCATION
        self.adjusted_target = EntityLocation()
        self.adjusted_yaw = 0
        self.min_movement_threshold = min_movement_threshold
        self.character = character #type: Character
        self.pitch_to_target = pitch_to_target
        self.distance_threshold = distance_threshold
        self.target_scale = target_scale
        self.distance_scale = distance_scale
        self.complete_on_execute_if_no_entity_found = complete_on_execute_if_no_entity_found

    def reset_target(self):
        self.target = NULL_LOCATION
        self.name = "move_to_entity:{}".format(self.target)
        if isinstance(self.target, NullLocation):
            if not isinstance(self.proposed_target, NullLocation):
                self.name = "move_to_entity:[proposed:{}]".format(self.proposed_target.get_short_entity_name())

        self.log_name = self.name

    def set_target(self, target):
        self.target = target
        self.name = "move_to_entity:{}".format(self.target)
        self.log_name = self.name
        logger.debug("{}: set target".format(self.log_name))

    @staticmethod
    def create_collect_drop(character: Character, item: str):
        return MoveToEntity(character,
                            item,
                            distance_threshold=0.3,
                            distance_scale=1,
                            pitch_to_target=True)

    def set_adjusted_target(self):
        if not isinstance(self.proposed_target, NullLocation):
            logger.debug("{}: setting adjusted target to proposed target".format(self.log_name))
            readonly_memory_object = self.character.get_observation(self.proposed_target.id)
            actual_location = readonly_memory_object.location
            if not isinstance(actual_location, NullLocation):
                logger.debug("{}: actual location of proposed target is now {}".format(self.log_name, actual_location))
                self.proposed_target.update(actual_location)
                self.entity_location = self.proposed_target
        else:
            logger.debug("{}: setting adjusted target".format(self.log_name))
            self.entity_location = self.character.get_closest_entity_location(self.entity_type)

        if isinstance(self.entity_location, NullLocation):
            logger.debug("EntityLocation was null, skipping")
            return
        #
        # if self.target.dist(self.entity_location) < 0.01:
        #     logger.debug("Target already set to closest")
        #     return

        self.set_target(self.entity_location)

        adjusted_x, adjusted_z, adjusted_yaw = self.character.find_best_location(self.target, block_size=self.target_scale)
        self.adjusted_yaw = adjusted_yaw
        self.adjusted_target.x = adjusted_x
        self.adjusted_target.y = self.target.y
        self.adjusted_target.z = adjusted_z
        self.adjusted_target.mc_type = self.target.mc_type
        self.adjusted_target.id = self.target.id

        self.name = "move_to_entity({}):{}".format(self.entity_type, self.adjusted_target)

    def is_complete(self):
        if isinstance(self.entity_location, NullLocation):
            self.set_adjusted_target()

        if not isinstance(self.entity_location, NullLocation):
            _, _, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)
            logger.debug("{}: distance is {} and threshold is {}".format(self.log_name, distance, self.distance_threshold))
            if distance < self.distance_threshold:
                logger.debug("{}: COMPLETE".format(self.log_name))
                self._completed = True
            else:
                logger.debug("{}: NOT COMPLETE".format(self.log_name))
                self._completed = False
        else:
            logger.debug("{}: Refusing to move to a NullLocation!".format(self.log_name))
            if self.complete_on_execute_if_no_entity_found:
                self._completed = True

        return self._completed

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        self.set_adjusted_target()
        if self.is_complete():
            return

        # TODO refactor this repated code to a method
        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)
        translation_velocity, rotation_velocity = self.character.calc_move_and_turn_velocities(distance, yaw,
                                                                                               distance_threshold=self.distance_threshold,
                                                                                               distance_scale=self.distance_scale)

        logger.debug("{}: translation_vel:{}, rotation_vel:{}".format(self.log_name, translation_velocity, rotation_velocity))

        if self.pitch_to_target:
            pitch_vel = self.character.calc_pitch_velocity(pitch)
            if abs(pitch_vel) > self.min_movement_threshold:
                self.character.command.set_pitch(pitch_vel)
            else:
                self.character.command.reset_pitch()

        if distance >= self.distance_threshold:
            logger.debug("{}: moving!!".format(self.log_name))
            translation = translation_velocity
            self.character.command.set_move(translation)
            self.character.command.set_turn(rotation_velocity)
            self._completed = False
        else:
            self.character.command.reset_move()
            self.character.command.reset_turn()
            self._completed = True

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class MoveNearEntity(MoveToEntity):
    def __init__(self, character: Character, mob_location: MobLocation):
        MoveToEntity.__init__(self, character, mob_location.mc_type, proposed_target=mob_location)


class MoveNearTarget(ActionBase):
    def __init__(self, character: Character,
                 target: Location = NULL_LOCATION,
                 target_type: str = "",
                 distance_threshold=0.1,
                 turn_threshold: float = 0.01,
                 target_scale = 1,
                 distance_scale = 2,
                 pitch_to_target=True):
        ActionBase.__init__(self, "move_near_target({})".format(target))
        self.target = target
        self.target_type = target_type
        self.adjusted_target = NULL_LOCATION
        self.look_target = self.target if not isinstance(self.target, Location) else self.target.copy()
        self.adjusted_yaw = 0
        self.turn_threshold = turn_threshold
        self.character = character  # type: Character
        self.pitch_to_target = pitch_to_target
        self.distance_threshold = distance_threshold
        self.target_scale = target_scale
        self.distance_scale = distance_scale

    def set_target(self, target: Location):
        self.target = target
        self.name = "move_near_target({})".format(self.target)
        self.log_name = self.name

    def set_adjusted_target(self):
        if isinstance(self.target, NullLocation):
            if self.target_type != "":
                types = MalmoTypeManager.get_types(self.target_type)
                if "mob" in types:
                    logger.debug("{}: attempting to locate entity type {}".format(self.log_name, self.target_type))
                    potential_target = self.character.get_closest_entity_location(self.target_type)
                else:
                    logger.debug("{}: attempting to locate block type {}".format(self.log_name, self.target_type))
                    potential_target = self.character.get_closest_block_location(self.target_type)

                if potential_target != NULL_LOCATION:
                    self.set_target(potential_target)
                    logger.debug("{}: found location {}".format(self.log_name, self.target))

        if isinstance(self.target, EntityLocation):
            self.set_adjusted_entity_target()
        else:
            self.set_adjusted_block_target()

    def set_adjusted_block_target(self):
        if not isinstance(self.adjusted_target, NullLocation):
            logger.debug("{}: Adjusted target is already set!".format(self.log_name))
            return

        current_loc = self.character.get_location()

        self.look_target.update(self.target)
        self.look_target.x = self.target.x + 0.5
        self.look_target.z = self.target.z + 0.5
        height_diff = self.target.y - current_loc.y
        logger.debug("{}: y_diff:{} character:{} look_target:{} ".format(self.log_name, height_diff, current_loc, self.look_target))
        if abs(height_diff) > 1:
            logger.error("{}: y offset is greater than 1; problems may result in identifying an adjusted target!".format(self.log_name))
        if height_diff == 0:
            logger.debug("{}: target is on the same level as character; look in the middle of the _bottom_".format(self.log_name))
        elif height_diff < 0:
            logger.debug("{}: target is below character; look in middle of the _top_".format(self.log_name))
            self.look_target.y = self.target.y + 1
        else:
            logger.debug("{}: target is above character; look in middle of _bottom_".format(self.log_name))
        logger.debug("{}: target:{} look_target:{}".format(self.log_name, self.target, self.look_target))

        positions_to_check = ((0, 1), (0, -1), (1, 0), (-1, 0))
        nearest_target = NULL_LOCATION
        nearest_target_distance = sys.maxsize
        for position in positions_to_check:
            location = self.target.copy()
            x_offset = position[0]
            z_offset = position[1]
            location.x += x_offset
            location.y = round(current_loc.y) #round up to int in case character is standing on a partial block (i.e., farmland)
            location.z += z_offset
            block_type = self.character.get_block_type_at(location)
            if block_type == "air":
                distance = current_loc.dist(location)
                if distance < nearest_target_distance:
                    logger.debug("{}: Location {} is closer".format(self.log_name, location))
                    nearest_target_distance = distance
                    nearest_target = location
            else:
                logger.debug("{}: Location {} is of type {} (not air) and will be skipped".format(self.log_name, location, block_type))

        if not isinstance(nearest_target, NullLocation):
            nearest_target.x += 0.5
            nearest_target.z += 0.5
            self.adjusted_target = nearest_target
            self.name = "move_near_target({}):{}:{}".format(location, self.adjusted_target, self.look_target)
        else:
            logger.debug("{}: nearest_target is null; clearing adjusted target!".format(self.log_name))
            self.adjusted_target = NULL_LOCATION

        logger.debug("{}: Set adjusted target to {}".format(self.log_name, self.adjusted_target))

    def set_adjusted_entity_target(self):
        if not isinstance(self.adjusted_target, NullLocation):
            logger.debug("{}: Adjusted target is already set!".format(self.log_name))
            return

        if isinstance(self.target, EntityLocation):
            current_loc = self.character.get_location()

            self.look_target.update(self.target)
            self.look_target.x = self.target.x + 0.5
            self.look_target.z = self.target.z + 0.5
            self.look_target.y = 1.2
            if self.target.mc_type.lower() == "chicken":
                self.look_target.y = 0.4
            logger.debug("{}: target:{} look_target:{}".format(self.log_name, self.target, self.look_target))

            positions_to_check = ((0, 2), (0, -2), (2, 0), (-2, 0))
            nearest_target = NULL_LOCATION
            nearest_target_distance = sys.maxsize
            for position in positions_to_check:
                location = self.target.copy()
                x_offset = position[0]
                z_offset = position[1]
                location.x += x_offset
                location.y = round(current_loc.y) #round up to int in case character is standing on a partial block (i.e., farmland)
                location.z += z_offset
                distance = current_loc.dist(location)
                if distance < nearest_target_distance:
                    logger.debug("{}: Location {} is closer".format(self.log_name, location))
                    nearest_target_distance = distance
                    nearest_target = location

            if not isinstance(nearest_target, NullLocation):
                nearest_target.x += 0.5
                nearest_target.z += 0.5
                self.adjusted_target = nearest_target
                self.name = "move_near_target({}):{}:{}".format(location, self.adjusted_target, self.look_target)
            else:
                logger.debug("{}: nearest_target is null; clearing adjusted target!".format(self.log_name))
                self.adjusted_target = NULL_LOCATION

            logger.debug("{}: Set adjusted target to {}".format(self.log_name, self.adjusted_target))

    def is_complete(self):
        if self._completed == False:
            self.set_adjusted_target()
            logger.debug("{}: target:{} adj:{} look:{}".format(self.log_name, self.target, self.adjusted_target, self.look_target))
            if not isinstance(self.adjusted_target, NullEntity):
                distance_complete = False
                yaw_complete = False
                pitch_complete = True
                target_yaw, target_pitch, target_distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)
                look_yaw, look_pitch, look_distance = self.character.calc_yaw_pitch_distance(self.look_target)
                logger.debug("{}: target_distance: {} threshold:{}".format(self.log_name, target_distance, self.distance_threshold))
                if target_distance < self.distance_threshold:
                    distance_complete = True

                look_yaw_vel = self.character.calc_yaw_velocity(look_yaw)
                pitch_str = ""
                if abs(look_yaw_vel) < self.turn_threshold:
                    yaw_complete = True
                if self.pitch_to_target:
                    pitch_complete = False
                    look_pitch_vel = self.character.calc_pitch_velocity(look_pitch)
                    if abs(look_pitch_vel) < self.turn_threshold:
                        pitch_complete = True
                    pitch_str = "look_pitch:{}".format(look_pitch_vel)
                logger.debug("{}: look_yaw:{} {} turn_threshold:{}".format(self.log_name, look_yaw_vel, pitch_str, self.turn_threshold))
                if distance_complete and yaw_complete and pitch_complete:
                    logger.debug("{}: action is complete!".format(self.log_name))
                    self._completed = True
                else:
                    logger.debug("{}: action is NOT complete!".format(self.log_name))

        return self._completed

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if self._completed:
            logger.debug("{}: Action is complete, returning without doing anything".format(self.log_name))
            return

        if isinstance(self.adjusted_target, NullLocation):
            logger.debug("{}: adjusted target is not set; refusing to move! (did you call is_complete() first?)".format(self.log_name))
            return

        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        target_move_vel, target_yaw_vel = self.character.calc_move_and_turn_velocities(distance, yaw,
                                                                        distance_threshold=self.distance_threshold,
                                                                        distance_scale=self.distance_scale)
        logger.debug("velocities t:{:.05f} y:{:.05f}".format(target_move_vel, target_yaw_vel))

        look_yaw, look_pitch, look_distance = self.character.calc_yaw_pitch_distance(self.look_target)

        if distance >= self.distance_threshold:
            logger.debug("{}: moving!".format(self.log_name))
            self.character.command.set_move(target_move_vel)
            self.character.command.set_turn(target_yaw_vel)
        else:
            self.character.command.reset_move()
            logger.debug("{}: Near adjusted target; now looking toward look target".format(self.log_name))
            look_yaw_vel = self.character.calc_yaw_velocity(look_yaw)
            if abs(look_yaw_vel) >= self.turn_threshold:
                logger.debug("{}: turning {}".format(self.log_name, look_yaw_vel))
                self.character.command.set_turn(look_yaw_vel)
            else:
                self.character.command.reset_turn()

        if self.pitch_to_target:
            logger.debug("{}: checking pitch".format(self.log_name))
            look_pitch_vel = self.character.calc_pitch_velocity(look_pitch)
            if abs(look_pitch_vel) >= self.turn_threshold:
                logger.debug("{}: pitching: {}".format(self.log_name, look_pitch_vel))
                self.character.command.set_pitch(look_pitch_vel)
            else:
                logger.debug("{}: pitch:{} within threshold:{}".format(self.log_name, look_pitch_vel, self.turn_threshold))
                self.character.command.reset_pitch()

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class MoveNearBlockType(MoveNearTarget):
    def __init__(self, character: Character,
                 target_type: str = "",
                 distance_threshold=2.0,
                 turn_threshold: float = 0.1):
        MoveNearTarget.__init__(self, character,
                                target = NULL_LOCATION,
                                target_type=target_type,
                                distance_threshold=distance_threshold,
                                turn_threshold=turn_threshold)


class MonitorNear(ActionBase):
    """Monitors a block target by periodically moving nearby to observe any changes.
       An external process can also call clear_monitor() to complete the goal.
       Once a change is noted, this monitor goal is completed.

       The visit_interval is provided in Minecraft (MC) time, which proceeds 1000 steps
       per MC hour with 1 day measuring 24000 steps and the day begining with zero at 0600.
       A default visit interval of 5000 will visit every 5 hours MC time, or about every 4 minutes wall-clock time.
       A default visit interval of 500 will visit every 50 seconds of MC time, or about every 25 seconds wall-clock time.

       A typical MC day lasts 20 minutes in wall-clock time (cf. https://minecraft.gamepedia.com/Day-night_cycle)
    """
    def __init__(self, character: Character, target: BlockLocation, monitor_for_type: str, visit_interval_mc_time=1000):
        ActionBase.__init__(self, "monitor_around({}, {})".format(target, monitor_for_type))
        self.character = character
        self.target = target
        self.monitor_for_type = monitor_for_type
        self.visit_interval_mc_time = visit_interval_mc_time

        self.has_observed_type = False
        self.next_visit_mc_time = 0
        self.move = MoveNearTarget(character, target=target, distance_threshold=5, turn_threshold=0.2)

    def clear_monitor(self):
        self._completed = True

    def is_complete(self):
        if self.character.has_observed(self.monitor_for_type):
            logger.debug("{}: has observed type of {}".format(self.log_name, self.monitor_for_type))
            self.has_observed_type = True
            self._completed = True

        return self._completed

    def is_active(self):
        current_mc_time = self.character.get_current_time()
        needs_visit = self.next_visit_mc_time <= current_mc_time
        if needs_visit:
            logger.debug("{}: visit needed, action is active")
            return True
        else:
            logger.debug("{}: visit not needed, action is inactive")
            return False

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if self.is_complete():
            logger.debug("{}: Action is complete, returning without doing anything".format(self.log_name))
            return

        current_mc_time = self.character.get_current_time()
        if self.move.is_complete():
            logger.debug("{}: monitor has visited target {}".format(self.log_name, self.target))
            self.next_visit_mc_time = current_mc_time + self.visit_interval_mc_time
            self.move.clear_complete()
        else:
            logger.debug("{}: monitor needs to visit target {}".format(self.log_name, self.target))
            self.move.execute()


class CollectFromLocation(MoveToStaticTarget):
    def __init__(self, character : Character,
                 item_name: str,
                 target: Location = NULL_LOCATION,
                 distance_threshold=0.3,
                 distance_scale=1,
                 pitch_to_target=True):
        MoveToStaticTarget.__init__(self, character,
                                    target,
                                    distance_threshold=distance_threshold,
                                    distance_scale=distance_scale,
                                    pitch_to_target=pitch_to_target)
        logger.debug("Constructed Collect({},{}".format(item_name, target))


class CollectDrop(MoveToEntity):
    def __init__(self, character : Character,
                 item_name: str,
                 distance_threshold=0.3,
                 distance_scale=1,
                 pitch_to_target=True,
                 complete_if_no_entity_found=True):
        MoveToEntity.__init__(self, character,
                              item_name,
                              distance_threshold=distance_threshold,
                              distance_scale=distance_scale,
                              pitch_to_target=pitch_to_target,
                              complete_on_execute_if_no_entity_found=complete_if_no_entity_found)
        logger.debug("{}: Collecting the following drop: {}".format(self.log_name, item_name))
        self.proposed_target = self.character.get_closest_drop_location(item_name)
        logger.debug("{}: Closest drop location: {}".format(self.log_name, self.proposed_target))
        self.name = "collect_drop:{}".format(item_name)
        self.log_name = self.name


class LookAtStaticTarget(ActionBase):
    def __init__(self, character : Character, target: Location = NULL_LOCATION, threshold : float = 0.01, adjusted_height = 0.0):
        ActionBase.__init__(self, "look_at_static:{}".format(target))
        self.character = character #type: Character
        self.target = target
        self.threshold = threshold

        self.adjusted_target = target if not isinstance(target, Location) else target.copy()
        self.adjusted_height = adjusted_height

    def reset_target(self):
        self.target = NULL_LOCATION
        self.name = "look_at_static:{}".format(self.target)
        self.log_name = self.name

    def set_target(self, target: Location):
        self.target = target
        self.name = "look_at_static:{}".format(self.target)
        self.log_name = self.name

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if isinstance(self.target, NullLocation):
            logger.debug("{}: Refusing to look at a NullLocation!".format(self.log_name))
            return

        adj_x, adj_y, adj_z = find_best_look_location(self.target)
        self.adjusted_target.x = adj_x
        self.adjusted_target.y = adj_y + self.adjusted_height
        self.adjusted_target.z = adj_z
        block_type = self.character.get_block_type_at(self.target)
        type_str = ""
        if block_type is not None:
            type_str = "({})".format(block_type)
        self.name = "look_at_static{}@{}".format(type_str, self.adjusted_target)

        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        pitch_vel = self.character.calc_pitch_velocity(pitch)
        pitch_within_tolerance = False
        if abs(pitch_vel) > self.threshold:
            self.character.command.set_pitch(pitch_vel)
        else:
            self.character.command.reset_pitch()
            pitch_within_tolerance = True

        yaw_vel = self.character.calc_yaw_velocity(yaw)
        yaw_within_tolerance = False
        if abs(yaw_vel) > self.threshold:
            self.character.command.set_turn(yaw_vel)
        else:
            self.character.command.reset_turn()
            yaw_within_tolerance = True

        logger.debug("{}: pitch:{:.5f} yaw:{:.5f} threshold{:.5f}".format(self.log_name, pitch_vel, yaw_vel, self.threshold))
        if pitch_within_tolerance and yaw_within_tolerance:
            self._completed = True

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class LookAtStem(LookAtStaticTarget):
    def __init__(self, character: Character, target: Location = NULL_LOCATION, threshold: float = 0.01):
        LookAtStaticTarget.__init__(self, character, target, threshold, adjusted_height=1.2)


class LookAtEntityType(ActionBase):
    def __init__(self, character: Character,
                 entity_type: str,
                 threshold: float = 0.01,
                 yaw_scale=90 ):
        ActionBase.__init__(self, "look_at_entity({})".format(entity_type))
        self.character = character #type: Character
        self.entity_type = entity_type
        self.yaw_scale = yaw_scale
        self.target = Location()
        self.adjusted_target = self.target.copy()
        self.threshold = threshold

    def set_adjusted_target(self):
        entity_location = self.character.get_closest_entity_location(self.entity_type)
        if isinstance(entity_location, NullEntity):
            return

        entity_height = 1.2
        if self.entity_type.lower() == "chicken":
            entity_height = 0.4

        adj_x, adj_y, adj_z = find_best_entity_look_location(self.target, height=entity_height)
        self.adjusted_target.x = adj_x
        self.adjusted_target.y = adj_y
        self.adjusted_target.z = adj_z
        self.name = "look_at_entity({}):{}".format(self.entity_type, self.adjusted_target)

        self.target.set(entity_location.x, entity_location.y, entity_location.z)

    def is_complete(self):
        if self._completed:
            logger.debug("{}: already complete, skipping".format(self.log_name))

        self.set_adjusted_target()
        pitch_within_threshold = False
        yaw_within_threshold = False

        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        pitch_vel = self.character.calc_pitch_velocity(pitch)
        if abs(pitch_vel) <= self.threshold:
            pitch_within_threshold = True

        yaw_vel = self.character.calc_yaw_velocity(yaw)
        if abs(yaw_vel) <= self.threshold:
            yaw_within_threshold = True

        if pitch_within_threshold and yaw_within_threshold:
            logger.debug("{}: COMPLETE".format(self.log_name))
            self._completed = True
        else:
            logger.debug("{}: NOT COMPLETE".format(self.log_name))
            self._completed = False

        return self._completed

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        self.set_adjusted_target()
        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        pitch_vel = self.character.calc_pitch_velocity(pitch)
        if abs(pitch_vel) > self.threshold:
            self.character.command.set_pitch(pitch_vel)
        else:
            self.character.command.reset_pitch()

        yaw_vel = self.character.calc_yaw_velocity(yaw)
        if abs(yaw_vel) > self.threshold:
            self.character.command.set_turn(yaw_vel)
        else:
            self.character.command.reset_turn()

        logger.debug("{}: pitch:{:.5f} yaw:{:.5f}".format(self.log_name, pitch_vel, yaw_vel))
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class LookAtEntity(LookAtEntityType):
    def __init__(self, character: Character,
                 entity_loc: EntityLocation,
                 threshold: float = 0.01,
                 yaw_scale=90):
        LookAtEntityType.__init__(self, character, entity_loc.mc_type, threshold, yaw_scale)
        self.target = entity_loc
        self.adjusted_target = self.target.copy()

    def set_adjusted_target(self):
        if not isinstance(self.target, NullLocation):
            logger.debug("{}: Adjusting look at target.".format(self.log_name))
            readonly_memory_object = self.character.get_observation(self.target.id)
            actual_location = readonly_memory_object.location
            if not isinstance(actual_location, NullLocation):
                logger.debug("{}: actual location of look at target is now {}".format(self.log_name, actual_location))
                self.target.update(actual_location)
            else:
                return
        else:
            return

        entity_height = 1.2
        if self.entity_type.lower() == "chicken":
            entity_height = 0.4

        adj_x, adj_y, adj_z = find_best_entity_look_location(self.target, height=entity_height)
        self.adjusted_target.x = adj_x
        self.adjusted_target.y = adj_y
        self.adjusted_target.z = adj_z
        self.name = "look_at_entity({}):{}".format(self.entity_type, self.adjusted_target)


class LookAtBlockType(ActionBase):
    def __init__(self, character: Character,
                 block_type: str,
                 threshold: float = 0.1,
                 yaw_scale=90):
        ActionBase.__init__(self, "look_at_block({})".format(block_type))
        self.character = character #type: Character
        self.block_type = block_type
        self.yaw_scale = yaw_scale
        self.target = Location()
        self.adjusted_target = self.target.copy()
        self.threshold = threshold

    def set_adjusted_target(self):
        block_location = self.character.get_closest_block_location(self.block_type)
        if isinstance(block_location, NullLocation):
            return

        adj_x, adj_y, adj_z = self.character.find_best_block_look_location(self.target)
        self.adjusted_target.set(adj_x, adj_y, adj_z)
        self.name = "look_at_entity({}):{}".format(self.block_type, self.adjusted_target)

        self.target.set(block_location.x, block_location.y, block_location.z)

    def is_complete(self):
        if self._completed:
            logger.debug("{}: already complete, skipping".format(self.log_name))

        self.set_adjusted_target()
        pitch_within_threshold = False
        yaw_within_threshold = False

        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        pitch_vel = self.character.calc_pitch_velocity(pitch)
        if abs(pitch_vel) <= self.threshold:
            logger.debug("{}: pitch {} within threshold {}".format(self.log_name, pitch_vel, self.threshold))
            pitch_within_threshold = True
        else:
            logger.debug("{}: pitch {} NOT within threshold {}".format(self.log_name, pitch_vel, self.threshold))


        yaw_vel = self.character.calc_yaw_velocity(yaw)
        if abs(yaw_vel) <= self.threshold:
            logger.debug("{}: yaw {} within threshold {}".format(self.log_name, yaw_vel, self.threshold))
            yaw_within_threshold = True
        else:
            logger.debug("{}: yaw {} NOT within threshold {}".format(self.log_name, yaw_vel, self.threshold))

        if pitch_within_threshold and yaw_within_threshold:
            logger.debug("{}: COMPLETE".format(self.log_name))
            self._completed = True
        else:
            logger.debug("{}: NOT COMPLETE".format(self.log_name))
            self._completed = False

        return self._completed

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        self.set_adjusted_target()
        yaw, pitch, distance = self.character.calc_yaw_pitch_distance(self.adjusted_target)

        pitch_vel = self.character.calc_pitch_velocity(pitch)
        if abs(pitch_vel) > self.threshold:
            self.character.command.set_pitch(pitch_vel)
        else:
            self.character.command.reset_pitch()

        yaw_vel = self.character.calc_yaw_velocity(yaw)
        if abs(yaw_vel) > self.threshold:
            self.character.command.set_turn(yaw_vel)
        else:
            self.character.command.reset_turn()

        logger.debug("{}: pitch:{:.5f} yaw:{:.5f}".format(self.log_name, pitch_vel, yaw_vel))
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class SelectHotbarSlot(ActionBase):
    '''Selects the hotbar slot.  '''
    def __init__(self, character : Character, slot : int):
        ActionBase.__init__(self, "select:{}".format(slot))
        self.slot = slot
        self.character = character #type: Character

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        selected = self.character.get_selected_hotbar_slot()

        self.character.command.enqueue_command("hotbar.{} 1".format(self.slot + 1))
        self.character.command.enqueue_command("hotbar.{} 0".format(self.slot + 1))
        self.character.set_selected_hotbar_slot(self.slot)
        self._completed = True

class SelectItem(ActionBase):
    '''Moves the item to the hotbar and selects it.  '''
    def __init__(self, character : Character, item_name: str):
        ActionBase.__init__(self, "select({})".format(item_name))
        self.character = character #type: Character
        self.item_name = item_name

    def is_complete(self):
        inventory = self.character.get_inventory()
        selected_index = self.character.get_selected_hotbar_slot()
        is_in_hotbar, hotbar_index = inventory.is_in_hot_bar(self.item_name)
        logger.debug("{}: item:{} index:{} selected:{}".format(self.log_name, self.item_name, hotbar_index, selected_index))
        if selected_index == hotbar_index:
            self._completed = True
            logger.debug("{}: COMPLETE".format(self.log_name))
        else:
            self._completed = False
            logger.debug("{}: NOT COMPLETE".format(self.log_name))

        return self._completed

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        inventory = self.character.get_inventory()
        if self.is_complete():
            logger.debug("{}: action complete, skipping".format(self.log_name))

        in_inventory = inventory.contains(self.item_name)
        if not in_inventory:
            logger.error("{}: Failed to select item {} because it is not in inventory!".format(self.log_name, self.item_name))
            self.is_consistent = False
        else:
            is_in_hotbar, hotbar_index = inventory.is_in_hot_bar(self.item_name)
            if is_in_hotbar:
                self.character.command.enqueue_command("hotbar.{} 1".format(hotbar_index + 1))
                self.character.command.enqueue_command("hotbar.{} 0".format(hotbar_index + 1))
                self.character.set_selected_hotbar_slot(hotbar_index)
            else:
                selected_index = self.character.get_selected_hotbar_slot()
                self.character.command.enqueue_command("swapInventoryItems {} {}".format(selected_index, inventory.index_of(self.item_name)))
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class Use(ActionBase):
    """Uses the active item."""

    def __init__(self, character : Character, item = None, target: Location = None ):
        ActionBase.__init__(self, "use")
        self.character = character #type: Character
        self.item_to_use = item
        self.target = target

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        self.character.command.enqueue_command("use 1")
        self.character.command.enqueue_command("use 0")
        self._completed = True
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class Attack(ActionBase):
    '''Uses the active item.  '''
    def __init__(self, character : Character, mc_object: str = ""):
        ActionBase.__init__(self, "attack")
        self.character = character #type: Character
        self.object = mc_object
        self.min_distance_from_agent = 3.0

    def execute(self):
        if self._completed:
            logger.debug("{}: already complete, skipping".format(self.log_name))
            return

        if isinstance(self.object, NullLocation):
            self.is_consistent = False
            return

        logger.debug("{}: Attacking the following entity at location: {}".format(self.log_name, self.object, type(self.object)))
        if isinstance(self.object, MobLocation):
            agent_location = self.character.get_latest_state().location
            logger.debug("{}: Current location of mob: {}, Agent location: {}".format(self.log_name, self.object,
                                                                                      agent_location))
            if self.object.dist(agent_location) > self.min_distance_from_agent:
                logger.debug("{}: Agent is too far away from target. Distance is {}. Replanning.".format(self.log_name,
                                                                                                         self.object.dist(agent_location)))
                self.is_consistent = False
                return

        logger.debug("{}: starting execute ".format(self.log_name))
        self.character.command.set_attack()

        self._completed = True
        logger.debug("{}: end of execute COMPLETE".format(self.log_name))


class SwapWithChest(ActionBase):
    def __init__(self, character : Character, chest_location: Location, chestIndex : int, characterInventoryIndex : int):
        ActionBase.__init__(self, "swap with chest")
        self.character = character
        self.inventoryIndex = int(float(characterInventoryIndex))
        self.containerIndex = int(float(chestIndex))

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        self.character.command.enqueue_command("swapInventoryItems {} chest:{}".format(self.inventoryIndex, self.containerIndex))
        self._completed = True
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class SwapWithChestByName(SwapWithChest):
    def __init__(self, character: Character, chestLocation: BlockLocation, itemToSwap: str):
        #Note: This doesnt take into account that the inventory might be full
        empty = character.get_latest_state().inventory.index_of("air")
        chestIndex = character.get_observed_chests()[chestLocation.id].index_of(itemToSwap)
        SwapWithChest.__init__(self, character, chestIndex, empty)

#TODO: Unify var names
class SwapInsideInventory(ActionBase):
    def __init__(self, character : Character, index1 : int, index2 : int):
        ActionBase.__init__(self, "swap")
        self.character = character
        self.index1 = index1
        self.index2 = index2

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        self.character.command.enqueue_command("swapInventoryItems {} {}".format(self.index1, self.index2))
        self._completed = True
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class Craft(ActionBase):
    def __init__(self, character : Character, item : str, colour : str=None):
        ActionBase.__init__(self, "craft_{}".format(item))
        self.character = character
        self.item = item
        self.colour = colour

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        colour_str = ""
        if self.colour is not None:
            colour_str = " {}".format(self.colour)
        self.character.command.enqueue_command("move 0")
        self.character.command.enqueue_command("turn 0")
        self.character.command.enqueue_command("craft {}{}".format(self.item, colour_str))
        self._completed = True
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class ExploreAreaWithSpiral(ComplexAction_DEPRECATED):
    """Explores an area starting in a circle of the perimeter and spiraling into the starting position.  """
    def __init__(self, character: Character, area=Area(min_x=-30, max_x=30, min_z=-30, max_z=30)):
        ComplexAction_DEPRECATED.__init__(self, "explore_spiral")
        self.character = character  # type: Character
        observation_grid = self.character.get_block_observation_grid()  #type: ObservationGridDetails
        self.x_range = observation_grid.x_range
        self.z_range = observation_grid.z_range

        self.min_x = area.min_x
        self.max_x = area.max_x
        self.min_z = area.min_z
        self.max_z = area.max_z
        self.x_size = self.max_x - self.min_x
        self.z_size = self.max_z - self.min_z
        self.start = Location()
        self.waypoints = list()
        self.completed_waypoints = list()

        self.move = MoveToStaticTarget(character, distance_threshold=3.0,
                                       distance_scale=2, pitch_to_target=True)

    def initialize_waypoints(self):
        current_loc = self.character.get_location()
        # middle_of_x_range = self.x_range / 2
        # middle_of_z_range = self.z_range / 2
        offset_x = current_loc.x
        offset_z = current_loc.z

        observation_x_size = int(self.x_size / self.x_range) + 1
        observation_z_size = int(self.z_size / self.z_range) + 1
        logger.debug("{}: Creating spiral for grid of size ({}, {})".format(self.log_name, observation_x_size, observation_z_size))
        positions = self.spiral(observation_x_size, observation_z_size)
        for position in positions:
            x_start = offset_x + (position[0] * self.x_range)
            z_start = offset_z + (position[1] * self.z_range)
            self.waypoints.append( (x_start, z_start))

    def spiral(self, X, Y):
        positions = list()
        x = y = 0
        dx = 0
        dy = -1
        for i in range(max(X, Y) ** 2):
            if (-X / 2 < x <= int(X / 2)) and (-Y / 2 < y <= int(Y / 2)):
                positions.append((x, y))

            if x == y or (x < 0 and x == -y) or (x > 0 and x == 1 - y):
                dx, dy = -dy, dx
            x, y = x + dx, y + dy
        return positions

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if len(self.waypoints) <= 0:
            self.initialize_waypoints()
            logger.debug("{}: Waypoints: {}".format(self.log_name, self.waypoints))

        if self._completed:
            logger.debug("{}: Action finished".format(self))
            return

        if isinstance(self.move.target, NullLocation):
            logger.debug("{}: Target is null, attempting to set the target to a new waypoint".format(self.log_name))
            if len(self.waypoints) > 0:
                waypoint = self.waypoints.pop()
                logger.debug("{}; Setting target to waypoint {}".format(self.log_name, waypoint))
                eye_height = self.character.get_actual_eye_height()
                self.move.set_target(Location(waypoint[0], eye_height, waypoint[1]))
            else:
                logger.debug("{}: No more waypoints; marking as completed".format(self.log_name))
                self._completed = True
        else:
            if self.move.is_complete():
                logger.debug("{}: Move to {} complete; resetting target to null".format(self.log_name, self.move.target))
                self.completed_waypoints.append(self.move.target.copy())
                self.move.target = NULL_LOCATION
                self.move.clear_complete()
            else:
                logger.debug("{}: Move to {} is not yet complete; executing it".format(self.log_name, self.move.target))
                self.move.execute()
                self.last_action = self.move

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class ExploreAreaWithSpiralTimeout(ExploreAreaWithSpiral):
    """Explores an area starting in a circle of the perimeter and spiraling into the starting position.  """
    def __init__(self, character: Character, area=Area(min_x=-30, max_x=30, min_z=-30, max_z=30)):
        ExploreAreaWithSpiral.__init__(self, character, area)
        self.character = character  # type: Character

        self.moveStart = -1
        self.timeout = 20000.0

        self.move = MoveSafelyToTarget(character, distance_threshold=3.0,
                                       distance_scale=2, pitch_to_target=True)

        self.create_waypoints = True

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if len(self.waypoints) <= 0 and self.create_waypoints:
            self.initialize_waypoints()
            self.create_waypoints = False
            logger.debug("{}: Waypoints: {}".format(self.log_name, self.waypoints))

        if self._completed:
            logger.debug("{}: Action finished".format(self))
            return

        if isinstance(self.move.target, NullLocation):
            logger.debug("{}: Target is null, attempting to set the target to a new waypoint".format(self.log_name))
            if len(self.waypoints) > 0:
                waypoint = self.waypoints.pop(0)
                logger.debug("{}; Setting target to waypoint {}".format(self.log_name, waypoint))
                eye_height = self.character.get_actual_eye_height()
                self.move.set_target(Location(waypoint[0], eye_height, waypoint[1]))
                self.moveStart = int(round(time.time() * 1000))
            else:
                logger.debug("{}: No more waypoints; marking as completed".format(self.log_name))
                self._completed = True
        else:
            if self.move.is_complete():
                logger.debug("{}: Move to {} complete; resetting target to null".format(self.log_name, self.move.target))
                self.completed_waypoints.append(self.move.target.copy())
                self.move.target = NULL_LOCATION
                self.move.clear_complete()

            elif self.timeout < int(round(time.time() * 1000)) - self.moveStart:
                # it seems like there should be a step to stop the current action from moving
                logger.debug("The action has timed out, added it back to the queue and moving to next point");
                self.waypoints.append(self.move.target.copy().as_array_xz())
                self.move.target = NULL_LOCATION
                self.move.clear_complete()
            else:
                logger.debug("{}: Move to {} is not yet complete; executing it".format(self.log_name, self.move.target))
                self.move.execute()
                self.last_action = self.move

        logger.debug("Time since last action: {}".format(int(round(time.time() * 1000)) - self.moveStart))
        logger.debug("The current waypoint list is: {}".format(self.waypoints))
        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class ExploreFor(ActionBase):
    def __init__(self, character: Character, item_name: str):
        ActionBase.__init__(self, "explore_for({})".format(item_name))
        self.character = character
        self.item_name = item_name
        self.remove_when_completed = False #Want this to force replanning
        self.explore_action = Explore(self.character)


    def is_complete(self):
        # NOTE: This action is never "completed," but is made inconsistent to force replanning
        if self.character.has_observed(self.item_name):
            self.is_consistent = False
        return False

    def execute(self):
        self.explore_action.execute()
        self.is_complete()



class Explore(ActionBase):
    '''TBD'''

    def __init__(self, character: Character, min_x=0, max_x=50, min_z=0, max_z=50):
        ActionBase.__init__(self, "explore")
        self.character = character  # type: Character
        self.min_x = min_x
        self.max_x = max_x
        self.min_z = max_z
        self.max_z = max_z
        self.time_limit = 30
        self.start_time = time.time()
        self.home = None
        self.return_home = None
        self.explore = None
        self.grid = [["unknown" for x in range(max_x - min_x + 1)] for z in range(max_z - min_z + 1)]
        self.done_exploring = False
        self.go_home = False

    def distance_and_yaw(self, x, z):
        current_loc = self.character.get_location()  # EntityLocation
        dx = x - current_loc.x
        dz = z - current_loc.z
        yaw = -180 * math.atan2(dx, dz) / math.pi
        distance = math.sqrt(dx * dx + dz * dz)
        logger.trace("{}: dx {} dz {} math atan2 {} math.pi {}".format(self.log_name, dx, dz, math.atan2(dx, dz), math.pi))
        logger.trace("{}: for Alex {},{} and target {},{}, distance is {} and yaw is {}".format(self.log_name, current_loc.x, current_loc.z, x, z, distance, yaw))
        return distance, yaw

    def estimate_time(self):
        current_loc = self.character.get_location()  # EntityLocation
        logger.trace("{}: estimating time; currently at {}, {} and home is at {}, {}".format(self.log_name, current_loc.x, current_loc.z, self.home.x, self.home.z))
        distance, yaw = self.distance_and_yaw(self.home.x, self.home.z)
        yaw = normalize_angle(yaw - current_loc.yaw)
        # guess 0.75 blocks per second?
        heuristic_distance = distance + abs(yaw / 90)
        return heuristic_distance

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        current_loc = self.character.get_location()  # EntityLocation
        if self.home == None:
            self.home = current_loc.copy()
            self.return_home = MoveToStaticTarget(self.character, self.home, distance_threshold=0.15, block_size=0)

        # first, update map from our current position
        # note that this doesn't really pay attention to what we are seeing
        # I was going to do that with a grid sensor but don't want to duplicate Irina's code
        # so here it just pays attention to what has been seen vs. not seen
        # this assumes you can see the 8 blocks surrounding you
        for x in range(-1, 2):
            for z in range(-1, 2):
                self.grid[math.floor(current_loc.x) + x][math.floor(current_loc.z) + z] = "known"

        if self._completed:
            logger.debug("{}: Goal is completed, skipping execution!".format(self.log_name))
            return

        elif self.return_home.is_complete():
            logger.debug("{}: done returning home".format(self.log_name))
            self._completed = True

        elif self.go_home or self.done_exploring or (
                time.time() + self.estimate_time() > self.start_time + self.time_limit):
            logger.debug("{}: done exploring? {}".format(self.log_name, self.done_exploring))
            logger.debug("{}: cur_time? {} time estimate? {} start_time? {} time_limit? {}".format(self.log_name, time.time(), self.estimate_time(), self.start_time, self.time_limit))
            logger.debug("{}: time to return home".format(self.log_name))
            self.go_home = True
            self.return_home.execute()

        # seems like a weird logic statement but don't want to send move command while already moving
        elif self.explore is None or self.explore.is_complete():
            # first, select next place to go
            # select unknown tile that's closest
            # this is not computationally efficient but with our small grid sizes it's good enough
            logger.debug("{}: creating new explore because reached old target!".format(self.log_name))
            self.explore = MoveToStaticTarget(self.character, Location(), distance_threshold=1.0, block_size=0)
            closest = []
            distance = sys.maxsize
            # TODO: Determine what 23 and 27 are
            for x in range(23, 27):
                for z in range(0, len(self.grid[0])):
                    if self.grid[x][z] == "unknown":
                        tmp_distance, yaw = self.distance_and_yaw(x + 0.5, z + 0.5)
                        yaw = normalize_angle(yaw - current_loc.yaw)
                        logger.debug("{}: tmp distance from {},{} to {},{} is {}; with yaw {} it is {}".format(self.log_name, current_loc.x, current_loc.z, x + 0.5, z + 0.5, tmp_distance, yaw, tmp_distance + abs(yaw / 30)))
                        # the 30 here is just a rough guess for how long it takes to turn
                        if tmp_distance + abs(yaw / 30) < distance:
                            closest.clear()
                            closest.append([x, z])
                            distance = tmp_distance
                        elif tmp_distance + abs(yaw / 30) == distance:
                            closest.append([x, z])
            if len(closest) == 0:
                logger.debug("{}: done exploring!!".format(self.log_name))
                self.done_exploring = True
            else:
                loc_index = random.randint(0, len(closest) - 1)
                logger.debug("{}: Alex is at {},{} and closest unexplored is {},{}".format(self.log_name, current_loc.x, current_loc.z, closest[loc_index][0], closest[loc_index][1]))
                self.explore.target.set(closest[loc_index][0], current_loc.y, closest[loc_index][1])
                self.explore.execute()
        elif not self.explore.is_complete():
            logger.debug("{}: exploring isn't done, executing without creating a new target".format(self.log_name))
            self.explore.execute()

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


class BreakUsingTimeoutInSeconds(ActionBase):
    '''Uses the active item.  '''
    def __init__(self, character : Character, time_in_seconds: float = 0.5):
        ActionBase.__init__(self, "break_using_timeout")
        self.character = character #type: Character
        self.time_in_seconds = time_in_seconds
        self.breaking = False
        self.start_break = 0

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if self._completed:
            logger.debug("{}: Action finished".format(self.log_name))
            return

        if not self.breaking:
            self.character.command.set_attack()
            self.start_break = datetime.datetime.now()
            logger.debug("{}: Starting".format(self.log_name))
            self.breaking = True
        else:
            now = datetime.datetime.now()
            time_delta = now - self.start_break
            logger.debug("{}: Run for {} seconds".format(self.log_name, time_delta.total_seconds()))
            if time_delta.total_seconds() >= self.time_in_seconds:
                self.character.command.reset_attack()
                self._completed = True
                logger.debug("{}: Finished".format(self.log_name))

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class BreakUsingLook(ActionBase):
    '''Uses the active item.  '''
    def __init__(self, character : Character, block_type = "", threshold=0.3):
        ActionBase.__init__(self, "break_using_look()")
        self.character = character #type: Character
        self.block_type = block_type
        self.threshold = threshold
        self.clear()

    def clear(self):
        self.start_break = 0
        self.starting_block = None
        self.clear_complete()

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))
        if self._completed:
            logger.debug("{}: Action finished".format(self.log_name))
            return

        look_hit_type = self.character.get_look_hit_type()
        look_in_range = self.character.get_look_in_range()
        look_distance = self.character.get_look_distance()
        if self.starting_block is None:
            if look_in_range:
                start_breaking = True
                if self.block_type != "":
                    if self.block_type != look_hit_type:
                        start_breaking = False

            if start_breaking:
                self.starting_block = look_hit_type
                self.starting_block_distance = look_distance
                self.character.command.set_attack(5)
                self.start_break = datetime.datetime.now()
                self.name = "break_using_look({})".format(self.starting_block)
                self.log_name = self.name
                logger.debug("{}: Starting hit on object {}".format(self.log_name, self.starting_block))
        else:
            current_distance = look_distance
            block_broken = current_distance > self.starting_block_distance + self.threshold
            logger.debug("{}: broken:{} start_d:{} (+{}) current_d:{}".format(self.log_name, block_broken, self.starting_block_distance, self.threshold, current_distance))
            if block_broken:
                self.character.command.reset_attack()
                self.starting_block = None
                self._completed = True
                now = datetime.datetime.now()
                time_delta = now - self.start_break
                logger.debug("{}: Completed in {} seconds".format(self.log_name, time_delta.total_seconds()))
                logger.debug("{}: Finished".format(self.log_name))
            else:
                self.character.command.set_attack(5)


        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class TestBreakBlock(ComplexAction_DEPRECATED):
    '''Breaks the target block by moving toward it, looking at it,
       and attacking it until it breaks.
    '''
    def __init__(self, character: Character, target: Location):
        ComplexAction_DEPRECATED.__init__(self, "break_block")
        self.character = character #type: Character
        self.move = MoveToStaticTarget(character, target, distance_threshold=2)
        self.look = LookAtStaticTarget(character, target)
        self.collect = MoveToStaticTarget.create_collect_from_location(self.character)
        self.break_block = BreakUsingLook(character)
        self.include_prefix = True

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))

        if self._completed:
            logger.debug("{}: Goal is completed, skipping execution!".format(self.log_name))
            self.last_action = None
            return

        block_type = self.break_block.starting_block
        if block_type is not None:
            drop_location = self.character.get_closest_drop_location(block_type)
            if isinstance(drop_location, DropLocation):
                logger.debug("{}: {} IS AROUND -- COLLECTING".format(self.log_name, block_type))
                self.collect.target = drop_location
                self.collect.execute()
                self.last_action = self.collect
                return

        if not self.move.is_complete():
            self.move.execute()
            self.last_action = self.move
            #TODO: make updating last_action more generic and part of the class hierarchy
        else:
            if not self.look.is_complete():
                self.look.execute()
                self.last_action = self.look
            else:
                if self.include_prefix:
                    logger.debug("{}: ==================================".format(self.log_name))
                    logger.debug("{}: ==================================".format(self.log_name))
                    self.include_prefix = False

                self.break_block.execute()
                self.last_action = self.break_block

                if self.break_block.is_complete():
                    logger.debug("{}: ++++++++++++++++++++++++++++++++++".format(self.log_name))
                    logger.debug("{}: ++++++++++++++++++++++++++++++++++".format(self.log_name))
                    logger.debug("{}: break block is finished".format(self.log_name))

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class TestBreakBlockType(ComplexAction_DEPRECATED):
    '''Breaks the target block by moving toward it, looking at it,
       and attacking it until it breaks.
    '''
    def __init__(self, character: Character, block_type: str):
        ComplexAction_DEPRECATED.__init__(self, "break_block({})".format(block_type))
        self.character = character #type: Character
        self.block_type = block_type
        self.target = NULL_LOCATION
        self.move = MoveToStaticTarget(character, self.target, distance_threshold=2, pitch_to_target=True)
        self.look = LookAtStaticTarget(character, self.target)
        self.break_block = BreakUsingLook(character)
        self.collect = MoveToStaticTarget.create_collect_from_location(self.character)
        self.include_prefix = True

    def reset_target(self):
        logger.debug("{}: resetting the target".format(self.log_name))
        self.target = NULL_LOCATION
        self.collect.reset_target()
        self.move.reset_target()
        self.look.reset_target()
        self.break_block.clear()
        self.name = "{}: break_block({})@{}".format(self.log_name, self.block_type, self.target)
        logger.debug("{}: ======================================================".format(self.log_name))
        logger.debug("{}: ======================================================".format(self.log_name))
        logger.debug("{}: ======================================================".format(self.log_name))

    def set_target(self, target):
        self.target = target
        self.move.set_target(target)
        self.look.set_target(target)
        self.name = "break_block({})@{}".format(self.block_type, self.target)
        logger.debug("{}: Set new target to {}".format(self.log_name, target))


    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))

        drop_location = self.character.get_closest_drop_location(self.block_type)
        if isinstance(drop_location, DropLocation):
            logger.debug("{}: {} drops are around -- collecting!".format(self.log_name, self.block_type))
            self.reset_target()
            self.collect.set_target(drop_location)
            self.collect.execute()
            self.last_action = self.collect
            return
        else:
            self.collect.clear_complete()
            self.collect.reset_target()

        if self._completed:
            logger.debug("{}: Goal is completed, skipping execution!".format(self.log_name))
            self.reset_target()
            self.last_action = None
            return

        closest_block = self.character.get_closest_block_location(self.block_type)
        if isinstance(closest_block, NullLocation):
            logger.debug(("{}: No blocks of type {} are known to character".format(self.log_name, self.block_type)))
            return
        else:
            logger.debug(("{}: Closest:{}".format(self.log_name, closest_block)))
            if self.target.dist(closest_block) != 0:
                new_target = BlockLocation()
                new_target.update(closest_block)
                self.set_target(new_target)
            else:
                logger.debug(("{}: closest already matches target".format(self.log_name)))


        if not self.move.is_complete():
            self.move.execute()
            self.last_action = self.move

        else:
            if not self.look.is_complete():
                self.look.execute()
                self.last_action = self.look
            else:
                if self.include_prefix:
                    logger.debug("{}: ==================================".format(self.log_name))
                    logger.debug("{}: ==================================".format(self.log_name))
                    self.include_prefix = False

                self.break_block.execute()
                self.last_action = self.break_block

                if self.break_block.is_complete():
                    logger.debug("{}: ++++++++++++++++++++++++++++++++++".format(self.log_name))
                    logger.debug("{}: ++++++++++++++++++++++++++++++++++".format(self.log_name))
                    logger.debug("break block is finished")
                    self.reset_target()
                    self.move.clear_complete()
                    self.look.clear_complete()
                    self.collect.clear_complete()

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))

class TestSlaughter(ComplexAction_DEPRECATED):
    '''**This action should only be used in test code.**
    ** ComplexAction is deprecated! Please ActionBase to create new Actions**

    Slaughters the closest entity of a given type by moving toward it, looking at it,
    and attacking it until it is no longer alive.  The same behavior could be created
       with an OrderedConjunctiveAction, but this design provides an abstract action.
    '''
    def __init__(self, character: Character, mob_type: str = "chicken", max_attempts=10):
        ComplexAction_DEPRECATED.__init__(self, "slaughter{}".format(mob_type))
        self.character = character #type: Character
        self.mob_type = mob_type
        self.meat_type = MalmoTypeManager.get_meat_type(mob_type)
        self.max_attempts = max_attempts
        self.attempts = 0
        self.select_sword = SelectItem(character, "iron_sword")
        self.move = MoveToEntity(character, mob_type, distance_threshold=1.5, distance_scale=2)
        self.look = LookAtEntityType(character, mob_type, threshold=0.05)
        self.collect = MoveToStaticTarget.create_collect_from_location(self.character)
        self.attack = Attack(character)

    def execute(self):
        logger.debug("{}: starting execute ".format(self.log_name))

        if self._completed:
            logger.debug("{}: Goal is completed, skipping execution!".format(self.log_name))
            self.last_action = None
            return

        meat_location = self.character.get_closest_drop_location(self.meat_type)
        if isinstance(meat_location, DropLocation):
            logger.debug("{}: MEAT IS AROUND -- COLLECTING".format(self.log_name))
            self.collect.target = meat_location
            self.collect.execute()
            return
        else:
            logger.debug("{}: no meat around".format(self.log_name))

        if not self.select_sword.is_complete():
            self.select_sword.execute()
            self.last_action = self.select_sword

        if not self.move.is_complete():
            self.move.execute()
            self.last_action = self.move
            self.look.clear_complete() #reset look if the entity moved
            self.attack.clear_complete()  # reset attack if the entity moved

        else:
            if not self.look.is_complete():
                self.look.execute()
                self.last_action = self.look
                self.attack.clear_complete()  #reset attack if the entity moved
            else:
                look_hit_type = self.character.get_look_hit_type()
                mob_type = self.mob_type
                if look_hit_type == mob_type:
                    self.attack.execute()
                    self.last_action = self.attack

        if self.attempts >= self.max_attempts:
            self._completed = True

        logger.debug("{}: end of execute completed:{}".format(self.log_name, self._completed))


