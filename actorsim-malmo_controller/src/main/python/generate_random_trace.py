import traceback

import XMLGenerator
import objectives_helper
from alex_behaviors import *
from character.character import CharacterState
from connector import MinecraftConnector

timeBetweenCommands = 0.05
debug_observations = 0
debug_new_items = 0
debug_actions = 0

startX = 0
startY = 0
startZ = 0
allowed_items = ["torch","ladder","iron_shovel","furnace","iron_helmet","iron_leggings","iron_chestplate","iron_boots","iron_sword","iron_pickaxe","sign","iron_hoe","bucket","paper"]

### Debug
errorLog = open("errorLog.txt","w")
iterationTimeUp = 30000
timeStart = 0
timeEnd = 0

l = list(permutations(["AB","CD","EF","GH","IJ","KL","MN","OP"]))

objectives_map = objectives_helper.read_objectives("objectives.txt")
objectives = list(objectives_map.keys())

character = CharacterState()
max_tries = 100

for iteration in range(max_tries):  #)objective_index in range(len(objectives)):
    print("========================================================================")  ###############################
    print("Setting up map")  ###############################
    permutationX = random.randint(0,len(l))
    worldMapFile = "3x3RoomVersion.txt"
    room_template = "mission.txt"
    with open(worldMapFile, "w") as f:
        with open(room_template) as f2:
            lines = f2.readlines()
            for lineNumber in range(0,len(lines)):
                f.write(lines[lineNumber])
        f.write("\n30 log {}\n".format(l[permutationX][0]))
        f.write("30 cobblestone {}\n".format(l[permutationX][1]))
        f.write("30 iron_ore {}\n".format(l[permutationX][2]))
        f.write("30 reeds {}\n".format(l[permutationX][3]))
        f.write("30 coal {}\n".format(l[permutationX][4]))
    startX = random.randint(11,16) + 0.5
    startY = 7
    startZ = random.randint(11,16) + 0.5

    print("Setting up objectives")  ###############################
    objective = objectives[iteration % len(objectives)]
    #objective = objectives[objective_index]
    #target_items = memory.get_items_from_priming(objective)
    target_items = list(objectives_map[objective])
    random.shuffle(target_items)
    print("(i:{})  {}: Target items set to {}".format(iteration, objective, target_items))

    print("Setting up mission")  ###############################
    timeout_ms = 30000 + (10000 * len(target_items))
    XMLGenerator.generateBuilding(worldMapFile, startX, startY, startZ, timeout_in_ms=timeout_ms)
    time.sleep(1)
    connector = MinecraftConnector()
    connector.initialize_minecraft()
    connector.start_sim_from_mission_file()
    print("Mission running ")

    q = queue.Queue()
    mapItems, agentStartX, agentStartZ = translateMap(worldMapFile)
    queueHolder = []

    inventory_changes = []

    timeStart = time.time()
    newGoal = True
    currentResource = ""
    currentRequestedAmount = 0
    craftingPhase = False
    all_items_crafted = False
    target_item = None
    while connector.world_state.is_mission_running \
          and not all_items_crafted :
        print(".", end="", flush=True)
        time.sleep(timeBetweenCommands)

        message = connector.get_observation()
        character.process_observation(message, debug_observations)

        if target_item == None:
            target_item = character.calculate_next_item_needed(target_items)
            if target_item != None:
                print("\nCrafting: {}".format(target_item), end="")
                item, resourceRequirements, craftingRequirements = fetchRecipeRequirements(target_item)
                craftingRequirements = processRecipeRequirements(craftingRequirements)
                ### RESOURCES TO COLLECT ###
                craftingQueue = queue.Queue()
                resourceQueue = queue.Queue()
                amountQueue = queue.Queue()
                cQueue = [] # CRAFTING ORDER QUEUE
                rQueue = [] # RESOURCE NAME QUEUE
                aQueue = [] # RESOURCE QUANTITY QUEUE
                for x in range(0,len(resourceRequirements),2):
                    rQueue.append(resourceRequirements[x])
                    aQueue.append(resourceRequirements[x+1])

                for x in range(0, len(rQueue)):
                    resourceQueue.put(rQueue[x])
                for x in range(0, len(aQueue)):
                    amountQueue.put(aQueue[x])
                craftingPhase = False

        if target_item == None:
            # no new target_item was needed, stop
            connector.agent_host.sendCommand('quit')
            print("\nquitting because all items are crafted in inventory".format(target_item))
            all_items_crafted = True
            continue

        for new_item in character.new_items:
            item_list = [ new_item ]
            for removed_item in character.removed_items:
                item_list.append(removed_item)
            inventory_changes.append(item_list)
            if debug_new_items:
                print("new:{}".format(character.new_items))
                print("removed:{}".format(character.removed_items))
                print("item_list:{}".format(item_list))

        if debug_new_items:
            item_list = []
            for new_item in character.new_items:
                item_list.append(new_item)
            for removed_item in character.removed_items:
                item_list.append(removed_item)
            print("new:{}".format(character.new_items))
            print("removed:{}".format(character.removed_items))
            print("item_list:{}".format(item_list))

        if target_item in character.items:
            target_items.remove(target_item)
            target_item = None
            continue

        if not craftingPhase:
            if newGoal == True:
                if debug_actions: print("looking for new resource")
                q = queue.Queue()
                currentResource = resourceQueue.get()
                currentRequestedAmount = amountQueue.get()
                #queueHolder = basicCollectResource(connector.world_state, mapItems, currentResource)
                queueHolder = basicCollectResource(character, mapItems, currentResource)
                for x in range(0, len(queueHolder)):
                    q.put(queueHolder[x])
                newGoal = False

            # TODO move to character class
            ob = None
            try:
                if message == '0':
                    ob = None
                    print ("Received '0' message")
                elif message == 0:
                    ob = None
                    print ("Received zero message")
                    continue
                else:
                    ob = json.loads(message)
            except Exception as e:
                print("PROBLEM: converting json for message '{}' failed because {}".format(message, e))
                traceback.print_exc()
            finally:
                if ob == None:
                    ob = dict()

            if ob == None:
                #message was blank, so skip processing this round
                continue

            if checkInventoryForItemAmount(ob,currentResource,currentRequestedAmount) or q.empty():
                q = queue.Queue()
                newGoal = True
                if debug_actions: print("GOT ENOUGH")
            if resourceQueue.empty() and q.empty() and newGoal == True:
                queueHolder = craftingRequirements
                for x in range(0, len(queueHolder)):
                    q.put(queueHolder[x])
                craftingPhase = True

        if not q.empty():
            command = q.get()
            if debug_actions: print(command)
            connector.agent_host.sendCommand(command)


    print()
    print("Mission ended")
    timeEnd = time.time()
    if (timeEnd - timeStart) > iterationTimeUp:
        errorLog.write("ERROR")

    with open('random_trace.txt', "a") as file:
        for item_list in inventory_changes:
            item_str = ""
            append = ""
            for item in item_list:
                item_str = "{}{}{}".format(item_str,append,item)
                append = ","

            file.write("{},{}\n".format(objective,item_str))
