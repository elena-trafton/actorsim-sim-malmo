from __future__ import print_function

from builtins import range
import MalmoPython
import os
import sys
import time
import json
import re
import queue as queue
import subprocess
import csv
import random
from itertools import permutations

commentRE    = re.compile("^#")
decoratRE    = re.compile("^([1-9][0-9]*) (block|item|entity)?:?([A-Za-z_]+) ([A-Z])([A-Z])")

# these commands were made to shortcut the repeated use and call of them

def movForward():
    return "move 1"
def movBackward():
    return "move -1"
def turnLeft():
    return "turn -1"
def turnRight():
    return "turn 1"

# when you travel to a resource area, the yaw is always correctly set for the survey to work
def travelToResourceArea(yaw, x1, z1, x2, z2, isStart):
    wentNorth = False
    directions = []
    while not(yaw == -90 or yaw == 90 or yaw == 0 or yaw == 180):
        yaw = yaw - 360
    # TURN TO GO NORTH OR SOUTH
    if z1 < z2:
        if yaw == 90:
            directions.append(turnLeft())
        if yaw == 180:
            directions.append(turnLeft())
            directions.append(turnLeft())
        if yaw == -90:
            directions.append(turnRight())
        # print("test1")
        wentNorth = False
    if z1 > z2:
        if yaw == 90:
            # print("1")
            directions.append(turnRight())
        if yaw == 0:
            # print("2")
            directions.append(turnLeft())
            directions.append(turnLeft())
        if yaw == -90:
            # print("this is what it should be doing")
            directions.append(turnLeft())
        # print(yaw)
        # print("test2")
        wentNorth = True
    if z1 == z2: # IF ALREADY AT CORRECT LATITUDE ASSUME WENT NORTH AND TURN AGENT
        if yaw == 90:
            directions.append(turnRight())
        if yaw == -90:
            directions.append(turnLeft())
        if yaw == 0:
            directions.append(turnLeft())
            directions.append(turnLeft())
        # print("test3")
        wentNorth = True

    # TRAVEL NORTH OR SOUTH
    for x in range (0, int(abs(z2-z1))):
        directions.append(movForward())

    # TURN TO GO EAST OR WEST
    if x1 < x2 and not wentNorth: # facing south, turn east
        directions.append(turnLeft())
    if x1 < x2 and wentNorth: # facing north, turn east
        directions.append(turnRight())
    if x1 > x2 and not wentNorth: # facing south, turn west
        directions.append(turnRight())
    if x1 > x2 and wentNorth: # facing north, turn west
        directions.append(turnLeft())

    # TRAVEL EAST OR WEST
    for x in range(0, int(abs(x2-x1))):
        directions.append(movForward())

    if isStart: # we want to face east
        if x1 > x2: # will already be facing west
            directions.append(turnLeft())
            directions.append(turnLeft())
        if x1 == x2: # will be facing north or south
            if wentNorth:
                directions.append(turnRight())
            else:
                directions.append(turnLeft())
    else: # we want to face west
        if x1 < x2: # will already be facing east
            directions.append(turnLeft())
            directions.append(turnLeft())
        if x1 == x2: # will be facing north or south
            if wentNorth:
                directions.append(turnLeft())
            else:
                directions.append(turnRight())
    return directions

#IMPLEMENT
def basicCollectResource(character, mapItems, resource):
    directions = []
    yaw = character.yaw
    x = character.location.x
    y = character.location.y
    z = character.location.z

    itemInfo,start,sx,sz,end,ex,ez = returnResourceInfo(mapItems,resource)
    # print("DISTANCE TO START")
    # print(abs(sx-x)+abs(sz-z))
    # print("DISTANCE TO END")
    # print(abs(ex-x)+abs(ez-z))
    if (abs(sx-x)+abs(sz-z)) < (abs(ex-x)+abs(ez-z)):
        directions += travelToResourceArea(yaw,x,z,sx,sz,True)
        directions += surveyArea(sx,sz,ex,ez,True)
        # print("TRAVELING TO THE START")
    else:
        directions += travelToResourceArea(yaw,x,z,ex,ez,False)
        directions += surveyArea(sx,sz,ex,ez,False)
        # print("TRAVELING TO THE END")

    return directions

# Return a row of the
def returnResourceInfo(mapItems, resource):
    for x in range(0,len(mapItems)):
        if mapItems[x][0] == resource:
            return mapItems[x]

#IMPLEMENT
def surveyArea(sx,sz,ex,ez,isStart):
    directions = []
    if isStart:
        for x in range(0, int(ez-sz)):
            for y in range(0, int(ex-sx)):
                directions.append(movForward())
            if x % 2 != 0:
                directions.append(turnLeft())
                directions.append(movForward())
                directions.append(turnLeft()) 
            else:
                directions.append(turnRight())
                directions.append(movForward())
                directions.append(turnRight())
        for y in range(0, int(ex-sx)):
            directions.append(movForward())
    else:
        for x in range(0, int(ez-sz)):
            for y in range(0, int(ex-sx)):
                directions.append(movForward())
            if x % 2 != 0:
                directions.append(turnLeft())
                directions.append(movForward())
                directions.append(turnLeft())   
            else:
                directions.append(turnRight())
                directions.append(movForward())
                directions.append(turnRight())
        for y in range(0, int(ex-sx)):
            directions.append(movForward())
    return directions

# def getCurrentPos(world_state):
#     yaw = (json.loads(world_state.observations[-1].text))['Yaw']
#     x = (json.loads(world_state.observations[-1].text))['XPos']
#     y = (json.loads(world_state.observations[-1].text))['YPos']
#     z = (json.loads(world_state.observations[-1].text))['ZPos']
#     return yaw,x,y,z

################################################################
#
# Inventory and State Space Commands
#
################################################################

# load map feature for future use of the map state space and knowledge
def loadMap(f):
    grid = []
    l = f.readline()
    maxRow = 0
    if l == "map\n":
        l = f.readline()
    while l and l != '\n':
        row = list(l)
        if row[-1] == '\n':
            row = row[:-1]
        maxRow = max(maxRow,len(row))
        grid.append(row)
        l = f.readline()
    return grid,maxRow

# loads an array of the available resources on the map
def loadDecorations(f):
    decorations = []
    l = f.readline()
    while l:
        m = decoratRE.match(l)
        if m:
            if valid( decorations, ord(m.group(4)), ord(m.group(5))):
                cat = m.group(2)
                if cat is None:
                    cat = 'item'
                val = {'count'    : int(m.group(1)),
                       'category' : cat,
                       'type'     : m.group(3),
                       'start'    : m.group(4),
                       'end'      : m.group(5)}
                decorations.append(val)
            else:
                print("Error: Invalid Decoration: " + l)
        l = f.readline()
    return decorations

def valid(d,s,e):
    isValid = s <= e
    if isValid:
        for entry in d:
            es = ord(entry['start'])
            ee = ord(entry['end'])
            if (es < s and ee > e) or (es >= s and es <= e) or (ee >= s and ee <= e):
                isValid = False
                break
    return isValid

# produces a detailed description of the items on the map and their locations
# FIX TO EXACT COORDINATES
def translateMap(roomFile):
    f = open(roomFile, "r")
    mapString, maxRow = loadMap(f) 
    # How mapItems is organized [map item type, start letter, x pos of that letter, y pos of that letter, end letter, x pos of that letter, y pos of that letter]
    mapItems = []
    mapItemFeatures = []
    mapItemFeatures += loadDecorations(f)
    agentStartX = 0
    agentStartZ = 0
    # print(mapItemFeatures)
    for x in range(0, len(mapItemFeatures)):
        mapItems.append([mapItemFeatures[x]['type'], mapItemFeatures[x]['start'], 0, 0, mapItemFeatures[x]['end'], 0, 0])
    # print(mapItems)
    for z in range(0, len(mapItems)):
        for x in range(0, len(mapString)):
            for y in range(0, len(mapString[0])):
                if mapItems[z][1] == mapString[x][y]: # sets up the start position of items relative to the txt map
                    mapItems[z][2] = y + 1.5 # 1.5 added to each to make up for difference between real time map coordinates and the txt map
                    mapItems[z][3] = x + 1.5
                if mapItems[z][4] == mapString[x][y]: # sets up the end position of items relative to the txt map
                    mapItems[z][5] = y + 1.5
                    mapItems[z][6] = x + 1.5
                if mapString[x][y] == 's':
                    agentStartX = y + 1.5
                    agentStartZ = x + 1.5

    return mapItems, agentStartX, agentStartZ

# this prints the agent's current inventory that can be accessed
def printInventory(obs):
    for i in range(0,9):
        key = 'InventorySlot_'+str(i)+'_item'
        var_key = 'InventorySlot_'+str(i)+'_variant'
        col_key = 'InventorySlot_'+str(i)+'_colour'
        if key in obs:
            item = obs[key]
            print(str(i) + " ------ " + item, end=' ')
        else:
            print(str(i) + " -- ", end=' ')
        if var_key in obs:
            print(obs[var_key], end=' ')
        if col_key in obs:
            print(obs[col_key], end=' ')
        print()

# function is given an observation and an item to be checked for
def checkInventoryForItem(obs, requested):
    print("#######################################################")
    print(obs)
    for i in range(0,39):
        key = 'InventorySlot_'+str(i)+'_item'
        if key in obs:
            item = obs[key]
            if item == requested:
                return True
    return False

# checks inventory for a requested item and a requested amount, if found in the inventory, return true
def checkInventoryForItemAmount(obs, requested, amount):
    for i in range(0,39):
        key = 'InventorySlot_'+str(i)+'_item'
        key2 = 'InventorySlot_'+str(i)+'_size'
        if key in obs:
            item = obs[key]
            quantity = obs[key2]
            if item == requested and int(quantity) >= int(amount):
                return True
    return False

def checkFuelPosition(obs, agent_host):
    '''Make sure our coal, if we have any, is in slot 0.'''
    # (We need to do this because the furnace crafting commands - cooking the potato and the rabbit -
    # take the first available item of fuel in the inventory. If this isn't the coal, it could end up burning the wood
    # that we need for making the bowl.)
    for i in range(1,39):
        key = 'InventorySlot_'+str(i)+'_item'
        if key in obs:
            item = obs[key]
            if item == 'coal':
                agent_host.sendCommand("swapInventoryItems 0 " + str(i))
                return

# returns 3 different arrays, the first being an orderd list of items that the agent has to collect, the next being the corresponding quantities of the items we want, and third the crafting order for the recipe.
def fetchRecipeRequirements(requestedItem):
    item = ""
    resourceRequirements = ""
    craftingRequirements = ""
    with open("recipes3x3.txt") as f:
        lines = f.readlines()
        data = [s.rstrip('\n') for s in lines]
        for i in range(0,len(data),3):
            if data[i] == requestedItem:
                item = data[i].split(',')
                resourceRequirements = data[i+1].split(',')
                craftingRequirements = data[i+2].split(',')
    return item, resourceRequirements, craftingRequirements

def processRecipeRequirements(recipeRequirements):
    craftingQueue = []
    executionTimes = 0
    currentItemToCraft = ""
    for x in range(0,len(recipeRequirements),2):
        currentItemToCraft = recipeRequirements[x]
        executionTimes = recipeRequirements[x+1]
        for y in range(0, int(executionTimes)):
            craftingQueue.append("craft {}".format(currentItemToCraft))
    return craftingQueue

