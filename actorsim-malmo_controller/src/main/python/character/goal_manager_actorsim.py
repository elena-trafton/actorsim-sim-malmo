import traceback

from threading import Thread, Condition

import time
from pymongo import MongoClient
from pymongo.change_stream import CollectionChangeStream
from pymongo.collection import Collection

from character.character import Character
from character.memory import ConvertWorldStateOptions
from loggers import MalmoLoggers
from mission import Mission

from utils.plan_state import PlanState

logger = MalmoLoggers.malmo_planner_logger
bindings_logger = MalmoLoggers.bindings_logger
trace_logger = MalmoLoggers.trace_logger

"""
A connector for controlling the character's goals from ActorSim.
"""
class ActorSimGoalManager():

    def __init__(self, character: 'Character', mission: Mission):
        self.character = character
        self.mission = mission
        self.mission_json = None
        self.name = character.name
        self.db_name = "mc_{}".format(self.name)

        self.log_name = "{}_goal_mgr".format(character.name)


        blockingTimeoutToWaitForConnectionInMS = 500
        self.client = MongoClient(serverSelectionTimeoutMS=blockingTimeoutToWaitForConnectionInMS)
        try:
            self.client.server_info()  # run a simple command to test the connection
        except:
            msg = "Connection to database {} failed.  Did you start the mongo server?".format(self.db_name)
            logger.error(msg)
            raise Exception(msg)

        self.db = self.client[self.db_name]

        self.pause_cognitive_cycle_during_debugging = False  # debug use only! will cause timing issues otherwise!
        self.post_character_observation_condition = Condition()
        self.worker_tread = ActorSimGoalManager.Worker(self, self.character)
        self.character.add_thread_to_start_with_mission(self.worker_tread)

        self.last_update_game_time = 0

        self.character.goal_manager = self  # NB: there can only be one goal_manager per character!
        self.character.add_post_hook_010_observation(self.post_observation_hook)

    def wait_for_actorsim(self):
        self.missions_collection = self.db.missions  # type: Collection
        if self.mission_json is None:
            self.mission_json = self.mission.toJSON()

        actorsim_started = False
        while not actorsim_started:
            if "_id" in self.mission_json:
                filter = {"_id": self.mission_json["_id"]}
                self.missions_collection.update_one(filter, self.mission_json)
            else:
                self.missions_collection.insert_one(self.mission_json)
            cursor = self.missions_collection.watch()  # type: CollectionChangeStream
            document = cursor.next()["fullDocument"]
            cursor.close()

            if "actorsim_ready" in document:
                actorsim_started = document["actorsim_ready"]

            time.sleep(0.5)

        logger.debug("ActorSim has returned the document {}".format(document))

    def post_observation_hook(self, character: 'Character'):
        logger.debug("Running CharacterController observation hook!")
        self.last_update_game_time = character.get_current_time()
        with self.post_character_observation_condition:
            self.post_character_observation_condition.notify()
            logger.debug("Notified goal manager of new state with index {}".format(self.character.get_latest_state().state_index))

    class Worker(Thread):
        def __init__(self, manager: 'ActorSimGoalManager', character : 'Character'):
            Thread.__init__(self)
            self.manager = manager
            self.character = character
            self.connector = character.connector
            old_name = self.getName()
            self.name="Cntlr_{}_{}".format(self.character.name, old_name)
            self.missions_collection = manager.db.missions  # type: Collection
            self.world_state_colleciton = manager.db.wm_world_state  # type: Collection
            self.observations_collection = manager.db.observations  # type: Collection
            self.plan_state_collection = manager.db.plan_states  #type: Collection

            self.last_observation_insert = None

        def run(self):
            if self.character.mission_running():
                self.export_options =  ConvertWorldStateOptions()

            while self.character.mission_running():
                with self.manager.post_character_observation_condition:
                    self.manager.post_character_observation_condition.wait()

                if self.manager.pause_cognitive_cycle_during_debugging:
                    self.character.cognitive_loop_countdown_latch.count_up(logger)

                try:
                    logger.info("{} notified... saving latest character details to database".format(self.name))

                    json_memory = self.character.toJSON(observation_reporting_interval=1)
                    json_memory["mission_id"] = self.mission_json["_id"]
                    if self.last_observation_insert is not None:
                        json_memory["previous_id"] = self.last_observation_insert["_id"]
                    self.observations_collection.insert_one(json_memory)
                    self.last_observation_insert = json_memory

                    state_index = json_memory["latest_state_index"]
                    plan_state = PlanState(self.character, state_index)
                    json_plan_state = plan_state.toJSON()
                    self.plan_state_collection.insert_one(json_plan_state)

                except Exception as e:
                    logger.error("{}: exception occurred:{}".format(self.name, e))
                    logger.error("{}: {}".format(self.name, traceback.format_exc()))

                if self.manager.pause_cognitive_cycle_during_debugging:
                    self.character.cognitive_loop_countdown_latch.count_down(logger)

            logger.debug("{} finished run".format(self.name))


