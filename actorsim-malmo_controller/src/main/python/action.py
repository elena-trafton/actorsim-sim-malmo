
import abc

from utils.LogManager import LogManager

logger = LogManager.get_logger('actorsim.malmo.action')


class ActionBase():
    '''An action for the character is a primitive action that can be completed with a somewhat simple
       state machine.   Examples would be look(target), move(target), break(target), hit(target), etc.

       All actions should extend ActionBase and implement the appropriate abstract methods.'''

    def __init__(self, name : str):
        self.name = name
        self.log_name = name
        self.remove_when_completed = True
        self._completed = False
        self.is_consistent = True  # during execution can be marked inconsistent with world state, thus not executable

    def __repr__(self):
        return "{}".format(self.name)

    def __repr__(self):
        completed_str = "_"
        if self._completed:
            completed_str = "x"
        return "{}{}".format(completed_str, self.name)

    def is_complete(self):
        '''States whether this action has declared itself complete.
        For convenience this method refers to a private variable _completed.
        However, clients should access _complated through this method so
        subclasses of ActionBase can override the method for distinct behavior.
        '''
        return self._completed

    def clear_complete(self):
        self._completed = False

    def is_ready_to_remove(self):
        return self.remove_when_completed and self._completed

    def is_active(self):
        """During execution, states whether this action is active.
           Inactive actions can take place when an action (or subaction)
           is paused waiting for environmental change.  For example, a
           maintence goal such as monitor_near(target, type) might be paused,
           or inactive, during the period when it has recently checked near
           the target but has not found anything of a particular type.
           During the period when the visit interval has not yet expired,
           the action is said to be inactive."""
        return True

    @abc.abstractmethod
    def execute(self):
        '''Performs the state machine associated with this action primitive.'''
        pass

class NullAction(ActionBase):

    def __init__(self, name="NULL_ACTION"):
        ActionBase.__init__(self, name)

    def execute(self):
        self._completed = True

NULL_ACTION = NullAction()
SKIP_ACTION = NullAction()

class ComplexAction_DEPRECATED(ActionBase):
    """**ComplexAction is deprecated! Please only use ActionBase to create
         new actions!**

       A complex action is one that composes multiple primitive actions together
       into something like a behavior. A ComplexAction is designed
       for  handcoded actions or used to collect a set of actions from a plan.

       **This class is deprecated! Please only use ActionBase to create
         new actions!**
    """
    def __init__(self, name):
        ActionBase.__init__(self, name)
        self.last_action = None #type: BaseAction


class OrderedConjunctiveAction(ComplexAction_DEPRECATED):
    '''**This action should only be used by a plan manager or test code.**
       A collection of actions that are to be completed in order.
       Only the first incomplete action is executed.

       **This action should only be used by a plan manager or test code.**
        '''
    def __init__(self, name, character=None):
        ComplexAction_DEPRECATED.__init__(self, name)
        self.character = character
        self.actions = list()  #type List[ActionBase]

    def __repr__(self):
        return self.to_str()

    def to_str(self, new_line_for_each_subaction=False):
        str = "{} complete:{} consistent:{}[".format(self.name, self._completed, self.is_consistent)
        sep = ""
        new_line = ""
        if new_line_for_each_subaction:
            new_line = "\n"
            sep = "  "
        for action in self.actions:
            str += "{}{}{}".format(new_line, sep, action)
            if not new_line_for_each_subaction:
                sep = ", "
        str += "]"
        return str

    def add(self, action : ActionBase):
        '''Adds action to the end of actions.'''
        self.actions.append(action)

    def is_active(self):
        """Confirms whether the next action is active."""
        next_action_active = True
        if len(self.actions) > 0:
            first_action = self.actions[0]
            if not first_action.is_active():
                next_action_active = False
        return next_action_active

    def execute(self):
        logger.debug("{}: executing {} actions".format(self.log_name, len(self.actions)))
        if self.character is not None:
            logger.debug("{}: Checking the command queue is empty before continuing.".format(self.log_name))
            if len(self.character.command.command_queue) > 0:
                logger.debug("{}: Command queue is not empty, skipping for now.".format(self.log_name))
                return

        for action in self.actions:
            if not action.is_complete():
                logger.debug("{}: next action incomplete; checking consistency of ({})".format(self.log_name, action.log_name))
                action_attempted = False
                if action.is_consistent:
                    logger.debug("{}: next action consistent; executing ({})".format(self.log_name, action.log_name))
                    action.execute()
                    self.last_action = action
                    self._completed = False
                    action_attempted = True

                if not action.is_consistent:
                    logger.debug("{}: next action inconsistent; marking self as inconsistent action:({})".format(self.log_name, action.log_name))
                    self.is_consistent = False
                    self._completed = False
                    action_attempted = True

                if action_attempted:
                    break #only execute the first incomplete action

        #keep nodes _not_ ready to remove; lhs: list's copy using colon as index, rhs: list comprehension testing removal
        self.actions[:] = [action for action in self.actions if not action.is_ready_to_remove()]
        if len(self.actions) == 0:
            self._completed = True
            logger.debug("{}: no more actions".format(self.log_name))
        else:
            all_subactions_completed = True
            for action in self.actions:
                if not action.is_complete():
                    all_subactions_completed = False
                    break
            if all_subactions_completed:
                logger.debug("{}: all subactions complete; marking self complete".format(self.log_name))
                self._completed = True
            else:
                logger.debug("{}: all subactions NOT complete; marking self NOT complete".format(self.log_name))
                self._completed = False

        if self._completed:
            logger.debug("{}: +++++ COMPLETED ".format(self.log_name))

