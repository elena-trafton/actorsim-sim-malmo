
import time
from threading import Thread

from utils.LogManager import LogManager
from character.character import Character

logger = LogManager.get_logger("spectator")

class Spectator(Character):

    def __init__(self):
        super(Spectator, self).__init__(**kwargs)
        self.grid_names = ['whole_map']
        self.worker_thread = Spectator.Worker(self)
        self.worker_thread.start()

    class Worker(Thread):
        def __init__(self, spectator: 'Spectator'):
            Thread.__init__(self)
            self.character = spectator
            self.connector = spectator.connector

        def run(self):
            last_print = 0
            while (self.connector.world_state.is_mission_running and self.character._keep_running):
                c = self.character
                s = c.latest_state
                logger.debug("Updating world observation")
                #slurp character state to kqml and update database

                time.sleep(self.parent.time_between_updates_in_ms)
