We recommend sticking with Ubuntu 16.04 until the installation is easier.  

But if you want to install on Ubuntu 18...

# Instructions for installing Malmo on Ubuntu 18 using Anaconda.  Also might be useful to those installing on OSX.


1) install Anaconda from anaconda.org.  Anaconda is some mix between a package manager and an virtual environment manager (this is my first time using it).  The installer is a bash shell script, and, for me, it installed everything in ~/anaconda3.  Anaconda installs Conda.

2) Use the Malmo Conda recipe here:

https://github.com/spMohanty/malmo-conda-recipe

You'll need to do a bit more than what it says under "Usage".

3) As the instructions on the page suggest, clone the recipe repo and cd into the directory,

    git clone https://github.com/spMohanty/malmo-conda-recipe
    cd malmo-conda-recipe

4) Read the README.md in malmo-conda-recipe.

you'll need to do:

    conda config --add channels conda-forge
    conda install -c crowdai malmo
    conda build .

If you don't add the conda-forge channel, the build will fail because the packages needed aren't in the default Conda repos.  At this point (or perhaps after a restart or logout/login) you should be able to run the Malmo server with malmo-server, or malmo-server -port 10001.  malmo-server should be in your path.  It might complain about some Gradle stuff, but it should still work.

5) The install will be in your anaconda3 folder in a folder called "install".  You can run the Python_Examples from there.  But you will need to set your MALMO_XSD_PATH to anaconda3/install/Schemas

export MALMO_XSD_PATH=/home/mroberts/anaconda3/install/Schemas


6) When you try to run any of the Python examples, you may see an error about a particular C++ symbol not being present or being the wrong version.  The solution I found online was to move the libstdc++ libraries that are in anaconda3/lib so that python would rely on your system libraries instead.

    cd anaconda3/lib
    mv libstdc++.so libstdc++.so.backup
    mv libstdc++.so.6 libstdc++.so.6.backup

7) You also need to install a few more python packages to run some of the Python examples such as mob_zoo.py or mob_fun.py

    pip install msgpack
    pip install future
    pip install pillow

8) You may need to update your ~/.bashrc (perhaps caused by running the anaconda installation under sudo?)



