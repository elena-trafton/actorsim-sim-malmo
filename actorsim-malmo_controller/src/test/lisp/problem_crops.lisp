(defproblem problem minecraft 
  (
   (type_potato potato)
   (type_crop potato)
   (type_seeds potato)
   (grows_from potato potato)

   (type_carrot carrot)
   (type_crop carrot)
   (type_seeds carrot)
   (grows_from carrot carrot)

   (type_beetroot beetroot)
   (type_crop beetroot)
   (type_seeds beetroot)
   (grows_from beetroot beetroot)

   (type_bone_meal bone_meal)
   ;; case 1: already have bread - ignored

   ;; case 2: use existing inventory
   ;; case 3: remove one of the below inventory to use placeholders
   (inventory_count 1 potato 1)
   (inventory_count 2 carrot 1)
;   (inventory_count 3 beetroot 1)
   (inventory_count 4 bone_meal 64)

   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)
   (inventory_open_index 9)
   (inventory_open_index 10)
   (inventory_open_index 11)
   (inventory_open_index 12)
   (inventory_open_index 13)
   (inventory_open_index 14)
   (inventory_open_index 15)
   (inventory_open_index 16)
   (inventory_open_index 17)
   (inventory_open_index 18)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; Observations
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (plantable farmland_001)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_beetroot)
   (type_beetroot placeholder_beetroot)
   (type_crop placeholder_beetroot)
   (type_seeds placeholder_beetroot)
   (grows_from placeholder_beetroot placeholder_beetroot)

   (placeholder placeholder_bone_meal)
   (type_bone_meal placeholder_bone_meal)

   (placeholder placeholder_farmland)
   (plantable placeholder_farmland)
   )
  (
   (obtain_potato)
   (obtain_carrots)
   (obtain_beetroot)
   )
)
