(defproblem problem minecraft 
  (
   (type_milk_bucket milk_bucket)
   (type_wheat wheat)
   (type_sugar sugar)
   (type_egg egg)

   ;; case 1: already have cake - ignored

   ;; case 2: use existing inventory
   ;; case 3: remove one of the below inventory to use placeholders
   (inventory_count 0 sugar 3)
   (inventory_count 1 wheat 6)
   (inventory_count 2 egg 12)
   (inventory_count 3 milk_bucket 3)

   (inventory_open_index 5)
   (inventory_open_index 6)
   (inventory_open_index 7)
   (inventory_open_index 8)
   (inventory_open_index 9)
   (inventory_open_index 10)
   (inventory_open_index 11)
   (inventory_open_index 12)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_sugar)
   (type_sugar placeholder_sugar)

   (placeholder placeholder_egg)
   (type_egg placeholder_egg)

   (placeholder placeholder_milk_bucket)
   (type_milk_bucket placeholder_milk_bucket)

   (placeholder placeholder_wheat)
   (type_wheat placeholder_wheat)
   )
  ((obtain_cake))
)
