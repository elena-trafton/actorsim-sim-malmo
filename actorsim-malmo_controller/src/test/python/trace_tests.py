import random
import unittest

import time

from utils.LogManager import LogManager
from actions import ExploreAreaWithSpiral
from area import Area
from character.action_trace import ActionTrace, PlanTrace

import test_missions
from character.character import Character
from character.factory import CharacterFactory, PlannerType, FoodPreference
from location import NULL_LOCATION, Location
from loggers import MalmoLoggers
from mission import ObservationGridDetails
from trace_manager import TraceManager

trace_logger = LogManager.get_logger('actorsim.malmo.test_trace')


class TraceTests(unittest.TestCase):

    def test_trace_pumpkin_breaking(self):
        mission_name = "test_pumpkin_breaking"
        mission = test_missions.get_simple_melon_mission(plant_pumpkin=True)
        alex_grid = ObservationGridDetails("Alex3x3").start(-20, -1, -20).size(40, 3, 40).relative() # allow Alex to see the pumpkins
        mission.observation_grids["Alex3x3"] = alex_grid

        mission.time_in_ms = 10000
        mission.start["Alex"].yaw = -90
        character = Character(mission)

        # TODO use a factory here to create the character with goal priorities
        #test_missions.get_mission_goals(mission_name, character)

        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = True

        character.start_mission()

        character.wait_for_item_in_inventory("pumpkin", desired=1)
        pumpkin_in_inventory = character.amount_of_item_in_inventory("pumpkin") > 0
        assert(pumpkin_in_inventory)

        trace_list = character.action_trace_list.get_and_reset()
        for trace in trace_list: #type: ActionTrace
            trace_logger.info("{}".format(trace))

    def test_trace_small_explore(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        seed = 2001
        random.seed(seed)

        mission = test_missions.get_test_farm_mission(0, 15, 0)

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid

        mission.time_in_ms = 30000
        mission.start["Alex"].yaw = -90
        character = Character(mission)

        area = Area(min_x=-40, max_x=40, min_z=-40, max_z=40)
        explore_goal = ExploreAreaWithSpiral(character, area)
        character.enqueue_action(explore_goal)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        character.start_mission()

        character.wait_on_mission_end()

        trace_list = character.action_trace_list.get_and_reset()
        for trace in trace_list: #type: ActionTrace
            if len(trace.newly_seen_items) > 0:
                trace_logger.info("{}".format(trace))

    def test_trace_explore(self):
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        seed = 1999
        random.seed(seed)

        mission = test_missions.get_test_farm_mission(0, 15, 0)

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid

        mission.time_in_ms = 90000
        mission.start["Alex"].yaw = -90
        mission.ms_per_tick = 20
        character = Character(mission)

        area = Area(min_x=-45, max_x=45, min_z=-45, max_z=45)
        explore_goal = ExploreAreaWithSpiral(character, area)
        character.enqueue_action(explore_goal)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        #MalmoLoggers.bindings_logger.setLevel(LogManager.INFO)
        character.start_mission()

        character.wait_on_mission_end()

        trace_list = character.action_trace_list.get_and_reset()
        for trace in trace_list: #type: ActionTrace
            if len(trace.newly_seen_items) > 0:
                trace_logger.info("{}".format(trace))

    def print_trace_history_during_mission(self, character, trace_manager, action_limit=30, sleep_time_in_seconds=5):
        while character.mission_running():
            time.sleep(sleep_time_in_seconds)
            action_limit = action_limit
            trace_logger.info("+=+=+= Printing the last {} primitive actions".format(action_limit))
            trace_subset = trace_manager.trace_list.get_recent_primitive_actions(limit=action_limit)
            for trace in trace_subset:
                target_str = "None"
                if trace.action_target != NULL_LOCATION:
                    target_str = "{}".format(trace.action_target)
                trace_logger.info("time:{} behavior:{} primitive:{} params:{} action:{} target:{}"
                                  .format(trace.mc_time,
                                    trace.behavior_name,
                                    trace.primitive_action_name,
                                    trace.primitive_action_params,
                                    trace.action_name,
                                    target_str))
                if len(trace.newly_seen_items) > 0:
                    trace_logger.info("time:{} newly_seen:{}".format(trace.mc_time, trace.newly_seen_items))

    def print_trace_resetting_each_time(self, character, trace_manager, sleep_time_in_seconds=5):
        while character.mission_running():
            time.sleep(sleep_time_in_seconds)
            trace_subset = trace_manager.get_and_reset()
            trace_logger.info("+=+=+= Printing the last {} primitive actions".format(len(trace_subset)))
            for trace in trace_subset:
                if isinstance(trace, ActionTrace):
                    target_str = "None"
                    if trace.action_target != NULL_LOCATION:
                        target_str = "{}".format(trace.action_target)
                    trace_logger.info("time:{} behavior:{} primitive:{} params:{} action:{} target:{}"
                                      .format(trace.mc_time,
                                        trace.behavior_name,
                                        trace.primitive_action_name,
                                        trace.primitive_action_params,
                                        trace.action_name,
                                        target_str))
                    if len(trace.newly_seen_items) > 0:
                        trace_logger.info("time:{} newly_seen:{}".format(trace.mc_time, trace.newly_seen_items))
                elif isinstance(trace, PlanTrace):
                    trace_logger.info("{}".format(trace))




    def test_omnivore_trace_shop(self):
        """Tests harvesting potatoes then exploring."""
        MalmoLoggers.malmo_root_logger.setLevel(LogManager.DEBUG)

        mission = test_missions.create_basic_chicken_mission(90000, center=Location(-30, 16, -30))
        mission.add_melon_farm(40, 15, 30, plant=False)

        alex_grid = ObservationGridDetails("Alex3x3").start(-10, -1, -10).size(20, 3, 20).relative()
        mission.observation_grids["Alex3x3"] = alex_grid
        mission.start["Alex"].x = 35
        mission.start["Alex"].z = 25
        mission.add_inventory_item(2, 'dye', 9, 'WHITE')

        factory = CharacterFactory()
        character = factory.create_character(mission, PlannerType.SHOP, food_preference=FoodPreference.OMNIVORE)

        trace_manager = TraceManager(character)

        MalmoLoggers.set_all_non_root_loggers(LogManager.ERROR)
        # MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        # MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        # MalmoLoggers.trace_logger.setLevel(LogManager.DEBUG)
        # #MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        # #MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        # # MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        # MalmoLoggers.log_observation_entities = False
        # MalmoLoggers.log_last_command = False

        character.start_mission()
        self.print_trace_resetting_each_time(character, trace_manager)

        character_trace_list = character.action_trace_list.get_and_reset()
        assert(len(character_trace_list) == 0)

        trace_manager.trace_list.print(trace_logger)

        assert(False)


    def test_fast_perimeter_explore_herbivore(self):
        """Walk a larger perimeter and behave like a herbivore."""
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.HERBIVORE)
        trace_manager = TraceManager(character)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        self.print_trace_history_during_mission(character, trace_manager)
        character.wait_on_mission_end()
        assert (False)


    def test_fast_perimeter_explore_omnivore(self):
        """Walk a larger perimeter and behave like a omnivore."""
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.OMNIVORE)
        trace_manager = TraceManager(character)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        self.print_trace_history_during_mission(character, trace_manager)
        character.wait_on_mission_end()
        assert (False)


    def trace_fast_bread_shop(self):
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.TEST_BREAD, explore_at_end=True)
        trace_manager = TraceManager(character)

        mission = character.mission
        test_missions.add_multiple_items(mission, "dye", count=15, colour="WHITE", center = Location(9, 16, -5), )
        test_missions.add_multiple_items(mission, "wheat_seeds", count=5, colour="WHITE", center = Location(9, 16, 5))
        mission.clear_inventory()
        mission.add_inventory_item(1, 'iron_sword', 1)
        # mission.add_inventory_item(2, 'dye', 12, 'WHITE')
        # mission.add_inventory_item(3, 'wheat_seeds', 3)
        mission.add_inventory_item(4, 'melon_seeds', 1)
        mission.add_inventory_item(5, 'beetroot_seeds', 1)
        mission.add_inventory_item(6, 'carrot', 1)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True
        MalmoLoggers.log_mission_spec = False

        character.start_mission()
        self.print_trace_history_during_mission(character, trace_manager)
        character.wait_on_mission_end()

        character.wait_on_mission_end()
        # TODO assert something useful here!
        assert(False)


