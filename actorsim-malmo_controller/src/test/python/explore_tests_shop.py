import unittest

import test_missions
from utils.LogManager import LogManager
from area import Area
from character.factory import CharacterFactory, PlannerType, FoodPreference
from loggers import MalmoLoggers, LogLevels
from mission import ObservationGridDetails


class ExploreTests(unittest.TestCase):
    # TODO new priority style: fix this unit test
    def test_fast_perimeter_explore_herbivore(self):
        """Walk a larger perimeter and behave like a herbivore."""
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.HERBIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_on_mission_end()
        assert (False)


    # TODO new priority style: fix this unit test
    def test_fast_perimeter_explore_carnivore(self):
        """Walk a larger perimeter and behave like a carnivore."""
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.CARNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_on_mission_end()
        assert (False)


    # TODO new priority style: fix this unit test
    def test_fast_perimeter_explore_omnivore(self):
        """Walk a larger perimeter and behave like a omnivore."""
        seed = 1999
        character = test_missions.setup_baseline_experiment_shop(seed, FoodPreference.OMNIVORE)

        MalmoLoggers.set_all_non_root_loggers(LogManager.INFO)
        MalmoLoggers.character_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.action_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.malmo_planner_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.jshop2_logger.setLevel(LogManager.DEBUG)
        MalmoLoggers.log_mission_spec = True
        MalmoLoggers.log_observation_entities = False
        MalmoLoggers.log_last_command = True

        character.start_mission()
        character.wait_on_mission_end()
        assert (False)

