

This allows you to run the test code without any connection to TensorFlow.

Python version: 3.6.2
MALMO version: 0.31.0 (might be worth trying on newer version)

Start the Minecraft client (see INSTALL.md for details of how to set this up, but ignore TensorFlow and CUDA setup for now).

I have tested this on Malmo 0.31.0 and, from now on, will use the variable 
MALMO_INSTALL = ~/virtualenv/glba/Malmo-0.31.0-Linux-Ubuntu-16.04-64bit_withBoost_Python3.5

``
cd MALMO_INSTALL/Minecraft
./launchClient.sh -port 10001
``

Then launch the test code:

``
mkdir test
cd test
ln -s ../src/startTemplateApple.sh
chmod 700 startTemplateApple.sh
./startTemplateApple.sh
``

Details about the parameters for running the challenge room are in startTemplateApple.sh, which also has a long comment about where to hook in your reactive controller.

